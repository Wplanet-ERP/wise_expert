<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('partner');
        $this->load->model('lector');
        $this->load->helper(array('url', 'expert'));
        $this->allow = array();
    }

	public function index()
	{
	    $session_c_no   = $this->session->userdata('ss_c_no');
	    $session_c_name = $this->session->userdata('ss_c_name');
        $session_c_email= $this->session->userdata('ss_c_email');
        $session_c_hp   = $this->session->userdata('ss_c_hp');
        $profile_data   = $this->lector->loadByCno($session_c_no);
        $company_data   = $this->partner->loadByCno($session_c_no);
        $profile_data['ss_c_type'] = $this->session->userdata('ss_c_type');

        $profile_data   = profileInitData($profile_data);

        if(empty($profile_data['name'])){
            $profile_data['name'] = $session_c_name;
        }

        if(!empty($profile_data['birthday']) && $profile_data['birthday'] != '0000-00-00')
        {
            $birthday_val_list = explode("-", $profile_data['birthday']);
            $profile_data['birthday1'] = isset($birthday_val_list[0]) ? $birthday_val_list[0] : "";
            $profile_data['birthday2'] = isset($birthday_val_list[1]) ? $birthday_val_list[1] : "";
            $profile_data['birthday3'] = isset($birthday_val_list[2]) ? $birthday_val_list[2] : "";
        }else{
            $profile_data['birthday1'] = "";
            $profile_data['birthday2'] = "";
            $profile_data['birthday3'] = "";
        }

        if(isset($profile_data['hp']) && !empty($profile_data['hp']))
        {
            $hp_val_list = explode("-", $profile_data['hp']);
            $profile_data['hp1'] = isset($hp_val_list[0]) ? $hp_val_list[0] : "010";
            $profile_data['hp2'] = isset($hp_val_list[1]) ? $hp_val_list[1] : "";
            $profile_data['hp3'] = isset($hp_val_list[2]) ? $hp_val_list[2] : "";
        }elseif(!empty($session_c_hp)){
            $hp_val_list = explode("-", $session_c_hp);
            $profile_data['hp1'] = isset($hp_val_list[0]) ? $hp_val_list[0] : "010";
            $profile_data['hp2'] = isset($hp_val_list[1]) ? $hp_val_list[1] : "";
            $profile_data['hp3'] = isset($hp_val_list[2]) ? $hp_val_list[2] : "";
        }else{
            $profile_data['hp1'] = "010";
            $profile_data['hp2'] = "";
            $profile_data['hp3'] = "";
        }

        if(isset($profile_data['tel']) && !empty($profile_data['tel']))
        {
            $tel_val_list = explode("-", $profile_data['tel']);
            $profile_data['tel1'] = isset($tel_val_list[0]) ? $tel_val_list[0] : "02";
            $profile_data['tel2'] = isset($tel_val_list[1]) ? $tel_val_list[1] : "";
            $profile_data['tel3'] = isset($tel_val_list[2]) ? $tel_val_list[2] : "";
        }else{
            $profile_data['tel1'] = "";
            $profile_data['tel2'] = "";
            $profile_data['tel3'] = "";
        }

        if(isset($profile_data['email']) && !empty($profile_data['email']))
        {
            $email_val_list = explode("@", $profile_data['email']);
            $profile_data['email1'] = isset($email_val_list[0]) ? $email_val_list[0] : "";
            $profile_data['email2'] = isset($email_val_list[1]) ? $email_val_list[1] : "";
        }elseif(!empty($session_c_email)){
            $email_val_list = explode("@", $session_c_email);
            $profile_data['email1'] = isset($email_val_list[0]) ? $email_val_list[0] : "";
            $profile_data['email2'] = isset($email_val_list[1]) ? $email_val_list[1] : "";
        }else{
            $profile_data['email1'] = "";
            $profile_data['email2'] = "";
        }

        if(isset($profile_data['expertise']) && !empty($profile_data['expertise']))
        {
            $profile_data['expertise_val'] = explode(",", $profile_data['expertise']);
        }else{
            $profile_data['expertise_val'] = "";
        }

        if(!empty($company_data['bk_title'])){
            $profile_data['bk_title'] = $company_data['bk_title'];
        }else{
            $profile_data['bk_title'] = "";
        }

        if(!empty($company_data['bk_name'])){
            $profile_data['bk_name'] = $company_data['bk_name'];
        }else{
            $profile_data['bk_name'] = "";
        }


        if(!empty($company_data['bk_num'])){
            $profile_data['bk_num'] = $company_data['bk_num'];
        }else{
            $profile_data['bk_num'] = "";
        }

        $mentor_list  = [];
        $license_list = [];
        $etc_list     = [];
        $career_list  = [];
        if(isset($profile_data['lp_no']) && !empty($profile_data['lp_no']))
        {
            $lp_no = $profile_data['lp_no'];

            #경력 데이터
            $career_row_list = $this->lector->loadCarrier($lp_no);
            $career_list     = !empty($career_row_list) ? $career_row_list : [];

            #자격증 데이터
            $license_row_list = $this->lector->loadLicense($lp_no);
            $license_list     = !empty($license_row_list) ? $license_row_list : [];

            #멘토 및 기타데이터
            $etc_row_list = $this->lector->loadEtc($lp_no);
            if(!empty($etc_row_list)){
                foreach($etc_row_list as $etc_data){
                    if($etc_data['lpe_type'] == '1'){
                        $mentor_list[] = $etc_data;
                    }elseif($etc_data['lpe_type'] == '2'){
                        $etc_list[] = $etc_data;
                    }
                }
            }
        }

        $profile_data['career_list']    = $career_list;
        $profile_data['license_list']   = $license_list;
        $profile_data['mentor_list']    = $mentor_list;
        $profile_data['etc_list']       = $etc_list;

        foreach($profile_data as $key => $value){
            if(!empty($value) && !is_array($value)){
                $profile_data[$key] = htmlspecialchars($value);
            }
        }

        $profile_data['birthday_list'] = getbirthdayList();
        $profile_data['hp_list']       = getHpList();
        $profile_data['tel_list']      = getTelList();
        $profile_data['expert_list']   = getExpertiseList();
        $profile_data['bank_list']     = getBankList();

        $this->load->view('inc/header');
        $this->load->view('inc/navibar');
        $this->load->view('partner/profile', $profile_data);
        $this->load->view('inc/non_footer');
	}

	public function profilePost()
    {
        $result         = false;
        $lp_no          = $this->input->post('lp_no');
        $post_data      = $this->input->post();
        $company_data   = getCompanyInitData($post_data);
        $lector_profile_data = getLectorInitData($post_data);


        # File 업로드
        $profile_img_file = isset($_FILES['profile_img']) ? $_FILES['profile_img'] : "";
        if(!empty($profile_img_file) && !empty($profile_img_file['name'])){
            $profile_img_path = store_image('profile_img', "partner");
            $profile_img_name = $profile_img_file['name'];

            $lector_profile_data['profile_img_path'] = $profile_img_path;
            $lector_profile_data['profile_img_name'] = $profile_img_name;
        }

        $bankbook_file = isset($_FILES['bankbook_file']) ? $_FILES['bankbook_file'] : "";
        if(!empty($bankbook_file) && !empty($bankbook_file['name'])){
            $bankbook_path = store_image('bankbook_file', "partner");
            $bankbook_name = $bankbook_file['name'];

            $lector_profile_data['bankbook_path'] = $bankbook_path;
            $lector_profile_data['bankbook_name'] = $bankbook_name;
            $company_data['r_accdoc'] = $bankbook_path;
            $company_data['o_accdoc'] = $bankbook_name;
        }

        $license_file = isset($_FILES['license_file']) ? $_FILES['license_file'] : "";
        if(!empty($license_file) && !empty($license_file['name'])){
            $license_path  = store_image('license_file', "partner");
            $license_name = $license_file['name'];

            $lector_profile_data['license_path'] = $license_path;
            $lector_profile_data['license_name'] = $license_name;
            $company_data['r_registration'] = $license_path;
            $company_data['o_registration'] = $license_name;
        }

        if(!empty($lp_no))
        {
            if($this->lector->updateMainData($lector_profile_data, $lp_no)){
                $result = true;
            }else{
                alert("오류가 발생했습니다. 다시 시도해주세요.");
            }
        }else{
            $lector_profile_data['c_no'] = $this->session->userdata('ss_c_no');
            $lector_profile_data['regdate'] = date("Y-m-d H:i:s");
            if($this->lector->insertMainData($lector_profile_data)){
                $result = true;
                $lp_no = $this->lector->getLastId();
            }else{
                alert("오류가 발생했습니다. 다시 시도해주세요.");
            }
        }

        if(!empty($lp_no) && $result)
        {
            # 파트너 데이터 company 테이블 업데이트
            $this->partner->updatePartnerData($company_data);

            #경력 초기화
            $this->lector->initSubTable($lp_no);

            #경력 저장
            $lpc_no_list            = !empty($this->input->post('lpc_no')) ? $this->input->post('lpc_no') : [];
            $lpc_name_list          = !empty($this->input->post('lpc_name')) ? $this->input->post('lpc_name') : [];
            $lpc_s_period_list      = !empty($this->input->post('lpc_s_period')) ? $this->input->post('lpc_s_period') : [];
            $lpc_e_period_list      = !empty($this->input->post('lpc_e_period')) ? $this->input->post('lpc_e_period') : [];
            $lpc_department_list    = !empty($this->input->post('lpc_department')) ? $this->input->post('lpc_department') : [];
            $lpc_position_list      = !empty($this->input->post('lpc_position')) ? $this->input->post('lpc_position') : [];
            $lpc_work_list          = !empty($this->input->post('lpc_work')) ? $this->input->post('lpc_work') : [];

            for($idx = 0; $idx < count($lpc_no_list) ; $idx++)
            {
                if(!empty($lpc_name_list[$idx]))
                {
                    $lpc_no         = trim($lpc_no_list[$idx]);
                    $lpc_name       = trim(addslashes($lpc_name_list[$idx]));
                    $lpc_s_period   = trim(addslashes($lpc_s_period_list[$idx]));
                    $lpc_e_period   = trim(addslashes($lpc_e_period_list[$idx]));
                    $lpc_department = trim(addslashes($lpc_department_list[$idx]));
                    $lpc_position   = trim(addslashes($lpc_position_list[$idx]));
                    $lpc_work       = trim(addslashes($lpc_work_list[$idx]));

                    if(!empty($lpc_no))
                    {
                        $lpc_upd_data = array(
                            'lpc_name'       => $lpc_name,
                            'lpc_s_period'   => $lpc_s_period,
                            'lpc_e_period'   => $lpc_e_period,
                            'lpc_department' => $lpc_department,
                            'lpc_position'   => $lpc_position,
                            'lpc_work'       => $lpc_work,
                            'lpc_display'    => '1'
                        );
                        $this->lector->updateCarrierData($lpc_upd_data, $lpc_no);
                    }else{
                        $lpc_ins_data = array(
                            'lp_no'          => $lp_no,
                            'lpc_name'       => $lpc_name,
                            'lpc_s_period'   => $lpc_s_period,
                            'lpc_e_period'   => $lpc_e_period,
                            'lpc_department' => $lpc_department,
                            'lpc_position'   => $lpc_position,
                            'lpc_work'       => $lpc_work,
                            'lpc_display'    => '1'
                        );
                        $this->lector->insertCarrierData($lpc_ins_data);
                    }
                }
            }

            #자격증 저장
            $lpl_no_list          = !empty($this->input->post('lpl_no')) ? $this->input->post('lpl_no') : [];
            $lpl_name_list        = !empty($this->input->post('lpl_name')) ? $this->input->post('lpl_name') : [];
            $lpl_entity_list      = !empty($this->input->post('lpl_entity')) ? $this->input->post('lpl_entity') : [];

            for($idx = 0; $idx < count($lpl_no_list) ; $idx++)
            {
                if(!empty($lpl_name_list[$idx]))
                {
                    $lpl_no         = trim($lpl_no_list[$idx]);
                    $lpl_name       = trim(addslashes($lpl_name_list[$idx]));
                    $lpl_entity     = trim(addslashes($lpl_entity_list[$idx]));

                    if(!empty($lpl_no))
                    {
                        $lpl_upd_data = array(
                            'lpl_name'     => $lpl_name,
                            'lpl_entity'   => $lpl_entity,
                            'lpl_display'  => '1'
                        );
                        $this->lector->updateLicenseData($lpl_upd_data, $lpl_no);
                    }else{
                        $lpl_ins_data = array(
                            'lp_no'        => $lp_no,
                            'lpl_name'     => $lpl_name,
                            'lpl_entity'   => $lpl_entity,
                            'lpl_display'  => '1'
                        );
                        $this->lector->insertLicenseData($lpl_ins_data);
                    }
                }
            }

            #강사/멘토 저장
            $mentor_lpe_no_list   = !empty($this->input->post('mentor_lpe_no')) ? $this->input->post('mentor_lpe_no') : [];
            $mentor_lpe_type_list = !empty($this->input->post('mentor_lpe_type')) ? $this->input->post('mentor_lpe_type') : [];
            $mentor_lpe_data_list = !empty($this->input->post('mentor_lpe_data')) ? $this->input->post('mentor_lpe_data') : [];

            for($idx = 0; $idx < count($mentor_lpe_no_list) ; $idx++)
            {
                if(!empty($mentor_lpe_data_list[$idx]))
                {
                    $lpe_no     = trim($mentor_lpe_no_list[$idx]);
                    $lpe_type   = trim($mentor_lpe_type_list[$idx]);
                    $lpe_data   = trim(addslashes($mentor_lpe_data_list[$idx]));

                    if(!empty($lpe_no))
                    {
                        $lpe_upd_data = array(
                            'lpe_type'     => $lpe_type,
                            'lpe_data'     => $lpe_data,
                            'lpe_display'  => '1'
                        );
                        $this->lector->updateEtcData($lpe_upd_data, $lpe_no);

                    }else{
                        $lpe_ins_data = array(
                            'lp_no'        => $lp_no,
                            'lpe_type'     => $lpe_type,
                            'lpe_data'     => $lpe_data,
                            'lpe_display'  => '1'
                        );
                        $this->lector->insertEtcData($lpe_ins_data);
                    }
                }
            }

            #기타 저장
            $etc_lpe_no_list   = !empty($this->input->post('etc_lpe_no')) ? $this->input->post('etc_lpe_no') : [];
            $etc_lpe_type_list   = !empty($this->input->post('etc_lpe_type')) ? $this->input->post('etc_lpe_type') : [];
            $etc_lpe_data_list   = !empty($this->input->post('etc_lpe_data')) ? $this->input->post('etc_lpe_data') : [];

            for($idx = 0; $idx < count($etc_lpe_no_list) ; $idx++)
            {
                if(!empty($etc_lpe_data_list[$idx]))
                {
                    $lpe_no     = trim($etc_lpe_no_list[$idx]);
                    $lpe_type   = trim($etc_lpe_type_list[$idx]);
                    $lpe_data   = trim(addslashes($etc_lpe_data_list[$idx]));

                    if(!empty($lpe_no))
                    {
                        $lpe_upd_data = array(
                            'lpe_type'     => $lpe_type,
                            'lpe_data'     => $lpe_data,
                            'lpe_display'  => '1'
                        );
                        $this->lector->updateEtcData($lpe_upd_data, $lpe_no);

                    }else{
                        $lpe_ins_data = array(
                            'lp_no'        => $lp_no,
                            'lpe_type'     => $lpe_type,
                            'lpe_data'     => $lpe_data,
                            'lpe_display'  => '1'
                        );
                        $this->lector->insertEtcData($lpe_ins_data);
                    }
                }
            }
        }

        alert("저장되었습니다", "/profile");
    }

    public function delFile()
    {
        $lp_no      = $this->input->post('lp_no');
        $file_type  = $this->input->post('file_type');
        $file_path  = $this->input->post('file_path');

        if(!empty($lp_no)){
            if($this->lector->delFile($lp_no, $file_type)){
                del_image($file_path);
            }else{
                alert("실패했습니다. 다시 시도해 주세요", "/profile");
            }
        }else{
            alert("오류가 발생했습니다. 다시 시도해 주세요", "/profile");
        }

        alert("삭제했습니다.", "/profile");
    }
}
