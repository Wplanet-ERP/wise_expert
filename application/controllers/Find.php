<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Find extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('partner');
        $this->load->model('sms');
        $this->load-> helper(array('date', 'expert'));

        if($this->session->userdata('ss_c_no')){
            redirect('/main');
        }
    }

	public function index()
	{
        redirect('find_id');
	}

    public function find_id()
    {
        $this->load->view('find/find_id');
        $this->load->view('inc/footer');
    }

    public function find_pw()
    {
        $this->load->view('find/find_pw');
        $this->load->view('inc/footer');
    }

    public function companyPwSearch()
    {
        $id     = $this->input->post('id');
        $c_name = $this->input->post('name');
        $result = false;
        $email  = "";
        $sms    = "";

        if(!empty($id) && !empty($c_name))
        {
            $comp_result = $this->partner->findPw($id, $c_name);

            $email_val  = isset($comp_result['tx_email']) ? $comp_result['tx_email'] : "";
            $sms_val    = isset($comp_result['tel']) ? $comp_result['tel'] : "";

            if(isset($comp_result['c_no']) && !empty($comp_result['c_no']))
            {
                $result = true;
            }

            if(!empty($email_val) && $email_val != "@")
            {
                $email_val_list = explode("@", $email_val);
                $email = substr($email_val_list[0], 0 , -2)."**@".$email_val_list[1];
            }

            if(!empty($sms_val))
            {
                $sms_last = substr($sms_val, -1);
                $sms = substr($sms_val, 0, -3) . "**" . $sms_last;
            }
        }

        $data = array("result" => $result, "email" => $email, "sms" => $sms);

        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }

    public function findPost()
    {
        $type        = $this->input->post('type');
        $result_type = "";

        if($type == 'find_id')
        {
            $f_email = $this->input->post('f_email');
            $f_name  = $this->input->post('f_name');

            $id_chk_result = $this->partner->findId($f_email, $f_name);

            if(isset($id_chk_result['id']) && !empty($id_chk_result['id']))
            {
                $this->session->set_userdata("find_id", $id_chk_result['id']);
                $result_type = "find_id_confirm";
            }else{
                $result_type = "find_id_fail";
            }
        }
        elseif($type == 'find_pw')
        {
            $f_id       = $this->input->post('f_id');
            $f_name     = $this->input->post('f_name');
            $send_type  = $this->input->post('send_type');

            $comp_result = $this->partner->findPw($f_id, $f_name);

            $c_no        = isset($comp_result['c_no']) ? $comp_result['c_no'] : "";
            $rand_str    = GenerateString(8);

            $send_result = false;
            if(!empty($c_no))
            {
                if ($send_type == 'email')
                {
                    $email    = isset($comp_result['tx_email']) ? $comp_result['tx_email'] : "";

                    $this->load->library('email');
                    $this->email->from("wplanet.ac@wplanet.co.kr", "와이즈 익스퍼트");
                    $this->email->to($email, $f_name);
                    $this->email->subject("임시비밀번호 발급입니다");
                    $this->email->message("<h2>임시비밀번호: <b>{$rand_str}</b></h2>");

                    if($this->email->send()){
                        $send_result = true;
                    }

                }
                elseif ($send_type == 'sms')
                {
                    $hp = isset($comp_result['tel']) ? $comp_result['tel'] : "";
                    if(sendSms($this->sms, $hp, $f_name, $rand_str)){
                        $send_result = true;
                    }
                }
            }

            if($send_result){
                $this->partner->partnerChangePassword($c_no, $rand_str);
                $result_type = "find_pw_confirm";
            }else{
                $result_type = "find_pw_fail";
            }
        }
        else
        {
            alert("잘못된 접근입니다.", "/login");
        }

        $this->session->set_userdata("result_type", $result_type);
        redirect("find/findResult");
    }

    public function findResult()
    {
        $result_type = $this->session->userdata('result_type');
        $this->session->unset_userdata('result_type');

        if($result_type == 'find_id_confirm')
        {
            $find_id_val = $this->session->userdata('find_id');
            $this->session->unset_userdata('find_id');

            $find_id      = substr($find_id_val, 0 ,-2)."**";
            $confirm_data = array(
                "find_id"    => $find_id
            );

            $this->load->view('find/find_id_confirm', $confirm_data);
            $this->load->view('inc/footer');

        }
        elseif($result_type == 'find_id_fail')
        {
            $this->load->view('find/find_id_fail');
            $this->load->view('inc/footer');
        }
        elseif($result_type == 'find_pw_confirm')
        {
            $this->load->view('find/find_pw_confirm');
            $this->load->view('inc/footer');
        }
        elseif($result_type == 'find_pw_fail')
        {
            $this->load->view('find/find_pw_fail');
            $this->load->view('inc/footer');
        }
        else
        {
            alert("잘못된 접근입니다.", "/login");
        }
    }
}
