<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('partner');
        $this->load-> helper(array('date'));

        if($this->session->userdata('ss_c_no')){
            redirect('/main');
        }
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $this->load->view('login');
        $this->load->view('inc/footer');
	}

	public function loginPost()
    {
        $user_id = $this->input->post('user_id');
        $pw      = $this->input->post('pw');
        
        if(!empty($user_id) && !empty($pw))
        {
            $getpwd  = substr(md5($pw),8,16);
            $partner_info = $this->partner->loadByIdPw($user_id, $getpwd);

            if($partner_info && isset($partner_info['c_no']) && !empty($partner_info['c_no'])){
                $partner = array(
                    "ss_c_no"       => $partner_info['c_no'],
                    "ss_c_id"       => $partner_info['id'],
                    "ss_c_name"     => $partner_info['c_name'],
                    "ss_c_type"     => $partner_info['license_type'],
                    "ss_c_email"    => $partner_info['tx_email'],
                    "ss_c_hp"       => $partner_info['tel'],
                );
                $this->session->set_userdata($partner);

                redirect('/main');
            }else{
                alert("등록된 파트너가 아닙니다.", "/login");
            }
            
        }else{
            alert("정확한 정보를 입력해주세요.", "/login");
        }
    }
}
