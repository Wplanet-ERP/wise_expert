<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('partner');
        $this->load->model('lector');
        $this->load->model('project');
        $this->load->helper(array('url', 'expert'));
        $this->allow = array();
    }

	public function index()
	{
        $partner = $this->partner->load();
        if(!empty($partner))
        {
            # 멘토링 및 프로필 작성여부 체크
            $profile_count    = $this->lector->profileCheck();
            $is_participation = ($partner['participation'] == '1') ? true : false;
            $is_partner_profile = ($profile_count > 0) ? true : false;

            # 강의 참여 리스트
            $partner_project_list = $this->project->getMainList();

            $mainData = array(
                "is_participation"      => $is_participation,
                "is_partner_profile"    => $is_partner_profile,
                "partner_project_list"  => $partner_project_list,
                "resource_type_list"    => getResourceTypeList(),
                "pj_participation_list" => getPjParticipation(),
            );

            $this->load->view('inc/header');
            $this->load->view('inc/navibar');
            $this->load->view('main', $mainData);
            $this->load->view('inc/non_footer');

        }else{
            $this->session->sess_destroy();
            alert('데이터가 없습니다. 다시 로그인 해주세요.', "/login");
        }
	}

    public function navibar_top()
    {
        $this->load->view('inc/navibar_top');
    }
}
