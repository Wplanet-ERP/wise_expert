<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set("display_errors", 1);

class Project_list extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('project');
        $this->load->model('lector');
        $this->load->helper(array('url', 'expert'));
        $this->allow = array();
    }

	public function index()
	{
        $session_c_no   = $this->session->userdata('ss_c_no');
        $session_c_name = $this->session->userdata('ss_c_name');
        $session_c_email= $this->session->userdata('ss_c_email');
        $session_c_hp   = $this->session->userdata('ss_c_hp');
        $profile_data   = $this->lector->loadByCno($session_c_no);
        $cur_date       = date('Y.m.d');

        if(empty($profile_data['name'])){
            $profile_data['name'] = $session_c_name;
        }

        if(empty($profile_data['department'])){
            $profile_data['department'] = "";
        }

        if(empty($profile_data['position'])){
            $profile_data['position'] = "";
        }

        if(isset($profile_data['hp']) && !empty($profile_data['hp']))
        {
            $hp_val_list = explode("-", $profile_data['hp']);
            $profile_data['hp1'] = isset($hp_val_list[0]) ? $hp_val_list[0] : "010";
            $profile_data['hp2'] = isset($hp_val_list[1]) ? $hp_val_list[1] : "";
            $profile_data['hp3'] = isset($hp_val_list[2]) ? $hp_val_list[2] : "";
        }elseif(!empty($session_c_hp)){
            $hp_val_list = explode("-", $session_c_hp);
            $profile_data['hp1'] = isset($hp_val_list[0]) ? $hp_val_list[0] : "010";
            $profile_data['hp2'] = isset($hp_val_list[1]) ? $hp_val_list[1] : "";
            $profile_data['hp3'] = isset($hp_val_list[2]) ? $hp_val_list[2] : "";
        }else{
            $profile_data['hp1'] = "010";
            $profile_data['hp2'] = "";
            $profile_data['hp3'] = "";
        }

        if(isset($profile_data['tel']) && !empty($profile_data['tel']))
        {
            $tel_val_list = explode("-", $profile_data['tel']);
            $profile_data['tel1'] = isset($tel_val_list[0]) ? $tel_val_list[0] : "02";
            $profile_data['tel2'] = isset($tel_val_list[1]) ? $tel_val_list[1] : "";
            $profile_data['tel3'] = isset($tel_val_list[2]) ? $tel_val_list[2] : "";
        }else{
            $profile_data['tel1'] = "";
            $profile_data['tel2'] = "";
            $profile_data['tel3'] = "";
        }

        if(isset($profile_data['email']) && !empty($profile_data['email']))
        {
            $email_val_list = explode("@", $profile_data['email']);
            $profile_data['email1'] = isset($email_val_list[0]) ? $email_val_list[0] : "";
            $profile_data['email2'] = isset($email_val_list[1]) ? $email_val_list[1] : "";
        }elseif(!empty($session_c_email)){
            $email_val_list = explode("@", $session_c_email);
            $profile_data['email1'] = isset($email_val_list[0]) ? $email_val_list[0] : "";
            $profile_data['email2'] = isset($email_val_list[1]) ? $email_val_list[1] : "";
        }else{
            $profile_data['email1'] = "";
            $profile_data['email2'] = "";
        }

        $profile_data['hp_list']       = getHpList();
        $profile_data['tel_list']      = getTelList();

        $resource_type_list = getResourceTypeList();
	    $project_list = $this->project->getProjectList($resource_type_list);
	    $project_data = $profile_data;
        $project_data["partner_project_list"]   = $project_list;
        $project_data["pj_participation_list"]  = getPjParticipation();
        $project_data["cur_date"]               = $cur_date;
        $ss_name_list = str_split($session_c_name, 3);
        $project_data['ss_name'] = implode(" ", $ss_name_list);

        $this->load->view('inc/header');
        $this->load->view('inc/navibar');
        $this->load->view('project/project_list', $project_data);
        $this->load->view('inc/non_footer');
	}

    public function cancelParticipation()
    {
        $pj_er_no = $this->input->post('pj_er_no');

        if($this->project->updatePjParticipation($pj_er_no, "3")){
            alert("참여 취소 되었습니다.", "/project_list");
        }else{
            alert("참여 취소에 실패했습니다. 다시 시도해 주세요.", "/project_list");
        }
    }

    public function approveParticipation()
    {
        $pj_er_no           = $this->input->post('pj_er_no');
        $confirm_hp1        = trim(addslashes($this->input->post('confirm_hp1')));
        $confirm_hp2        = trim(addslashes($this->input->post('confirm_hp2')));
        $confirm_hp3        = trim(addslashes($this->input->post('confirm_hp3')));
        $confirm_hp         = $confirm_hp1."-".$confirm_hp2."-".$confirm_hp3;
        $confirm_tel1       = trim(addslashes($this->input->post('confirm_tel1')));
        $confirm_tel2       = trim(addslashes($this->input->post('confirm_tel2')));
        $confirm_tel3       = trim(addslashes($this->input->post('confirm_tel3')));
        $confirm_tel        = $confirm_tel1."-".$confirm_tel2."-".$confirm_tel3;
        $confirm_email1     = trim(addslashes($this->input->post('confirm_email1')));
        $confirm_email2     = trim(addslashes($this->input->post('confirm_email2')));
        $confirm_email      = $confirm_email1."@".$confirm_email2;

        $confirmData = array(
            "pj_er_no"           => $this->input->post('pj_er_no'),
            "confirm_name"       => trim(addslashes($this->input->post('confirm_name'))),
            "confirm_department" => trim(addslashes($this->input->post('confirm_department'))),
            "confirm_position"   => trim(addslashes($this->input->post('confirm_position'))),
            "confirm_hp"         => $confirm_hp,
            "confirm_tel"        => $confirm_tel,
            "confirm_email"      => $confirm_email,
            "confirm_date"       => date('Y.m.d')
        );

        if($this->project->insertPjParticipationConfirm($confirmData) && $this->project->updatePjParticipation($pj_er_no, "2")){
            alert("참여 수락 되었습니다", "/project_list");
        }else{
            alert("참여 수락에 실패했습니다. 다시 시도해 주세요.", "/project_list");
        }
    }
}
