<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Regist extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('partner');
        $this->load-> helper(array('date', 'expert'));

        if($this->session->userdata('ss_c_no')){
            redirect('/main');
        }
    }

	public function index()
	{
        $this->load->view('regist');
        $this->load->view('inc/footer');
	}

	public function step01()
    {
        $this->load->view('regist/step01');
    }

    public function step02()
    {
        $step02_data = array(
            "email_list"         => getEmailList(),
            "hp_list"            => getHpList(),
            "license_type_list"  => getLicenseType(),
            "participation_list" => getParticipationList(),
        );
        $this->load->view('regist/step02', $step02_data);
    }

    public function step03(){
        $this->load->view('regist/step03');
    }

    public function registPost()
    {
        $f_password = trim($this->input->post('f_password'));
        $f_password_convert = substr(md5($f_password),8,16);
        $f_email    = addslashes(trim($this->input->post('f_email1'))."@".trim($this->input->post('f_email2')));
        $f_hp       = trim($this->input->post('f_hp1'))."-".trim($this->input->post('f_hp2'))."-".trim($this->input->post('f_hp3'));

        $registData = array(
            'f_ip'      => ip2long($_SERVER['REMOTE_ADDR']),
            'f_id'      => trim($this->input->post('f_id')),
            'f_pass'    => $f_password_convert,
            'f_name'    => addslashes(trim($this->input->post('f_name'))),
            'f_email'   => $f_email,
            'f_hp'      => $f_hp,
            'f_license_type'      => trim($this->input->post('f_license_type')),
            'f_tx_company_number' => trim($this->input->post('f_tx_company_number')),
            'f_tx_company'        => trim($this->input->post('f_tx_company')),
            'f_tx_company_ceo'    => trim($this->input->post('f_tx_company_ceo')),
            'f_participation'     => trim($this->input->post('f_participation')),
        );

        if($this->partner->insert($registData)){
            redirect('/regist/step03');
        }else{
            alert("오류가 발생했습니다. 다시 시도해주세요.",'step02');
        }
    }

    public function companyIdChk()
    {
        $id     = $this->input->post('id');
        $result = false;
        if(!empty($id)){
            $partner_id_count = $this->partner->partnerCheck($id);

            if($partner_id_count == 0){
                $result = true;
            }
        }

        $data = array("result" => $result);

        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }
}
