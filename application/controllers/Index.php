<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller
{
    function __construct() {
        parent::__construct();
        if($this->session->userdata('ss_c_no')){
            redirect('/main');
        }
    }

	public function index()
	{
        redirect('/login');
	}
}
