<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mypage extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('partner');
        $this->load->helper(array('url', 'expert'));
        $this->allow = array();
    }

	public function index()
	{
        $my_info = $this->partner->load();
        $my_info['participation_list'] = getParticipationList();
        $this->load->view('inc/header');
        $this->load->view('inc/navibar');
        $this->load->view('partner/mypage', $my_info);
        $this->load->view('inc/non_footer');
	}

    public function changePassword()
    {
        $partner_pw_new = $this->input->post('partner_pw_new');

        if($this->partner->partnerChangePassword($this->session->userdata('ss_c_no'), $partner_pw_new)){
            $pw_result_msg = "비밀번호가 변경되었습니다.";
        }else{
            $pw_result_msg = "비밀번호 변경에 실패했습니다.";
        }

        alert($pw_result_msg, "/mypage");
    }

    public function leavePartner()
    {
        $this->load->view('inc/header');
        $this->load->view('inc/navibar');
        $this->load->view('partner/leave_partner');
        $this->load->view('inc/non_footer');
    }

    public function leavePost()
    {
        $leave_reason = $this->input->post('leave_reason');

        if($this->partner->leavePartner($leave_reason)){
            $pw_result_msg = "탈퇴신청 되었습니다.";
        }else{
            $pw_result_msg = "탈퇴신청에 실패했습니다. 다시 시도해 주세요.";
        }

        alert($pw_result_msg, "/mypage");
    }

    public function save_el()
    {
        $process = $this->input->post("process");
        $value   = $this->input->post("value");

        if($process == 'modify_participation')
        {
            if($this->partner->partUpdate($value)){
                echo "변경되었습니다.";
            }else{
                echo "변경에 실패했습니다.";
            }
            exit;
        }
    }
}
