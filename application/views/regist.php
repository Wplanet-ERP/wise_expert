<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />

    <title>Create Expert</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- 합쳐지고 최소화된 최신 자바스크립트 -->
    <script src="https://code.jquery.com/jquery-1.9.1.min.js" charset="utf-8"></script>
    <script	src="/js/jquery-ui.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link href="/css/expert/regist.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker3.min.css">
    <script	type="text/javascript" src="/js/bootstrap/bootstrap-datepicker.min.js"></script>
    <script	type="text/javascript" src="/js/bootstrap/bootstrap-datepicker.ko.js"></script>

    <script	src="/js/jquery.chained.min.js"></script>
    <script	src="/js/jquery.chained.remote.min.js"></script>
</head>
<body>
<div id="continer">
    <iframe src="/regist/step01" id="step-iframe" width="460" height="630" frameborder="0"></iframe>
