<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />

    <title>Create Incubating</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- 합쳐지고 최소화된 최신 자바스크립트 -->
    <script src="https://code.jquery.com/jquery-1.9.1.min.js" charset="utf-8"></script>
    <script	src="/js/jquery-ui.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link href="/css/expert/regist.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker3.min.css">
    <script	type="text/javascript" src="/js/bootstrap/bootstrap-datepicker.min.js"></script>
    <script	type="text/javascript" src="/js/bootstrap/bootstrap-datepicker.ko.js"></script>

    <script	src="/js/jquery.chained.min.js"></script>
    <script	src="/js/jquery.chained.remote.min.js"></script>
</head>
<body>
    <ul class="regist-step">
        <li class="step">[Step 01]<br/>약관동의</li>
        <li class="label">>></li>
        <li class="step">[Step 02]<br/>개인정보 입력</li>
        <li class="label">>></li>
        <li class="step active">[Step 03]<br/>가입완료</li>
    </ul>

    <div class="finishFrm">
        <h2>가입 완료되었습니다.<br/><br/>로그인 하시겠습니까?</h2>
        <button type="button" class="btn-dark" onclick="moveLoginPage()">확인</button>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#step-iframe", parent.document).height(360);
        });

        function moveLoginPage()
        {
            parent.document.location.href = "/login";
        }
    </script>
</body>
</html>
