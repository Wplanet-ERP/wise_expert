<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />

    <title>Create Incubating</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- 합쳐지고 최소화된 최신 자바스크립트 -->
    <script src="https://code.jquery.com/jquery-1.9.1.min.js" charset="utf-8"></script>
    <script	src="/js/jquery-ui.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link href="/css/expert/regist.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker3.min.css">
    <script	type="text/javascript" src="/js/bootstrap/bootstrap-datepicker.min.js"></script>
    <script	type="text/javascript" src="/js/bootstrap/bootstrap-datepicker.ko.js"></script>

    <script	src="/js/jquery.chained.min.js"></script>
    <script	src="/js/jquery.chained.remote.min.js"></script>
    <script	src="/js/expert/regist_v1.js"></script>
</head>
<body>
    <ul class="regist-step">
        <li class="step">[Step 01]<br/>약관동의</li>
        <li class="label">>></li>
        <li class="step active">[Step 02]<br/>개인정보 입력</li>
        <li class="label">>></li>
        <li class="step">[Step 03]<br/>가입완료</li>
    </ul>

    <form action="/regist/registPost" name="registFrm" class="registFrm" method="post" onsubmit="return checkForm()">
        <h2 class="regist-title">● 개인정보 입력</h2>

        <div class="form-group">
            <label for="f_id" class="regist-label">*아이디</label>
            <div class="input-wrapper">
                <input type="text" class="form-control" name="f_id" id="f_id" placeholder="아이디" required>
                <button type="button" onclick="checkId()" class="regist-check-btn">중복체크</button>
                <input type="hidden" id="f_id_chk" value="0" />
            </div>
        </div>
        <div class="form-group">
            <label for="f_password" class="regist-label">*비밀번호 <span class="font_gray_01">6~20자의 영문,숫자,특수문자 조합 사용</span></label>
            <div class="input-wrapper">
                <input type="password" class="form-control" name="f_password" id="f_password" placeholder="비밀번호" autocomplete="off" required>
            </div>
        </div>
        <div class="form-group">
            <label for="f_password_confirm" class="regist-label">*비밀번호 확인</label>
            <div class="input-wrapper">
                <input type="password" class="form-control" name="f_password_confirm" id="f_password_confirm" placeholder="비밀번호 확인" autocomplete="off" required>
            </div>
        </div>
        <div class="form-group">
            <label for="f_name" class="regist-label">*이름</label>
            <div class="input-wrapper">
                <input type="text" class="form-control" name="f_name" id="f_name" placeholder="이름" required>
            </div>
        </div>
        <div class="form-group">
            <label for="f_email1" class="regist-label">*이메일</label>
            <div class="input-wrapper">
                <input type="text" class="form-control" name="f_email1" id="f_email1" required>
                <span class="f_email_tlt">@</span>
                <input type="text" class="form-control" name="f_email2" id="f_email2" required>
                <select id="select-email" class="form-control" >
                    <option value="">::선택::</option>
                    <?php foreach($email_list as $label){ ?>
                        <option value="<?=$label?>"><?=$label?></option>
                    <?php }?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="f_hp1" class="regist-label">휴대폰</label>
            <div class="input-wrapper">
                <select name="f_hp1" class="form-control" id="f_hp1">
                    <?php foreach($hp_list as $label){ ?>
                    <option value="<?=$label?>"><?=$label?></option>
                    <?php } ?>
                </select>
                <input type="text" class="form-control" name="f_hp2" id="f_hp2" maxlength="4">
                <input type="text" class="form-control" name="f_hp3" id="f_hp3" maxlength="4">
            </div>
        </div>
        <div class="form-group">
            <label class="regist-label">*강의(기타비용 포함) 수령방법</label>
            <div class="input-wrapper" style="padding-top: 10px;">
                <input type="radio" name="f_tuition_method" class="f_tuition_method" id="f_tuition_method_1" value="1" required>
                <label for="f_tuition_method_1" style="margin-right: 10px;">세금계산서</label>
                <input type="radio" name="f_tuition_method" class="f_tuition_method" id="f_tuition_method_2" value="2" required>
                <label for="f_tuition_method_2">계좌이체</label>
            </div>
        </div>
        <div class="form-group">
            <label class="regist-label">*법인/개인구분</label>
            <div class="input-wrapper" style="padding-top: 10px;">
                <?php foreach($license_type_list as $key => $label){ ?>
                    <input type="radio" name="f_license_type" class="f_license_type" id="f_license_type_<?=$key?>" value="<?=$key?>" required>
                    <label for="f_license_type_<?=$key?>" class="f_license_type_label" id="f_license_type_label_<?=$key?>"><?=$label?></label>
                <?php } ?>
                <div class="f_license_type_detail">
                    <div>
                        <label for="f_tx_company_number" class="col-form-label" >*사업자등록번호</label>
                        <input type="text" class="form-control" id="f_tx_company_number" name="f_tx_company_number">
                    </div>
                    <div>
                        <label for="f_tx_company" class="col-form-label" >*회사명</label>
                        <input type="text" class="form-control" id="f_tx_company" name="f_tx_company">
                    </div>
                    <div>
                        <label for="f_tx_company_ceo" class="col-form-label" >*대표자명</label>
                        <input type="text" class="form-control" id="f_tx_company_ceo" name="f_tx_company_ceo">
                    </div>
                </div>
                <br/>
                <span class="f_participation_notice">*법인회사, 개인회사는 강사비를 세금계산서로 받는 경우에만 선택합니다.</span>
            </div>
        </div>
        <div class="form-group">
            <label class="regist-label">*강의/멘토링 참여 여부</label>
            <div class="input-wrapper" style="padding-top: 10px;">
                <?php foreach($participation_list as $key => $label){ ?>
                <input type="radio" name="f_participation" class="f_participation" id="f_participation_<?=$key?>" value="<?=$key?>" required>
                <label for="f_participation_<?=$key?>"><?=$label?></label>
                <?php } ?>
                <br/>
                <span class="f_participation_notice">※ 강사 프로필의 연락처를 통해 강의/멘토링 참여 요청 연락이 진행됩니다.</span>
            </div>
        </div>

        <button type="submit" class="btn-dark">다음</button>
    </form>
</body>
</html>
