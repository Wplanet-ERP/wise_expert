<div class="profile-wrapper" style="min-width:440px;">
    <form name="profile_frm" id="profile_frm" method="post" action="/profile/profilePost" enctype="multipart/form-data" style="margin: 5px 0;">
        <input type="hidden" name="lp_no" value="<?=$lp_no?>">

        <div class="profile-logo-wrapper">
            <img class="profile-logo" src="/images/profile_logo.png">
            <img class="profile-incu-logo" src="/images/profile_incu.png">
        </div>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th colspan="5" class="profile-title">●강사 프로필</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td rowspan="5" class="profile-img-wrapper">
                        <label for="profile-img" class="profile-img-label">
                            <span class="preview-img-span" style="<?php if(!empty($profile_img_path)){?>display: none;<?php } ?>">[사진등록]<br/><br/>권장사이즈 : 103*132픽셀 (2MB 이내)</span>
                            <img src="<?php if(!empty($profile_img_path)){?>/uploads/<?=$profile_img_path?><?php }else{ ?>#<?php } ?>" class="preview-img" accept="image/*" data-type='image' onerror="this.style.display='none'">
                        </label>
                        <input type="file" name="profile_img" id="profile-img">
                    </td>
                    <td class="profile-label">
                        <label for="department">소속</label>
                    </td>
                    <td class="profile-input">
                        <input type="text" name="department" id="department" class="profile-text" value="<?=$department?>">
                    </td>
                    <td class="profile-label">
                        <label for="position">직위</label>
                    </td>
                    <td class="profile-input">
                        <input type="text" name="position" id="position" class="profile-text" value="<?=$position?>">
                    </td>
                </tr>
                <tr>
                    <td class="profile-label">
                        <label for="name">성명</label>
                    </td>
                    <td class="profile-input">
                        <input type="text" name="name" id="name" class="profile-text" value="<?=$name?>">
                    </td>
                    <td class="profile-label">
                        <label for="birthday1">생년월일</label>
                    </td>
                    <td class="profile-input">
                        <input type="text" name="birthday1" id="birthday1" class="profile-birthday-text" value="<?=$birthday1?>" maxlength="4">
                        <select name="birthday2" class="profile-birthday-select" >
                            <option value="">월</option>
                            <?php foreach($birthday_list as $label){?>
                                <option value="<?=$label?>" <?php if($birthday2 == $label){ ?>selected<?php } ?>><?=$label?></option>
                            <?php } ?>
                        </select>
                        <input type="text" name="birthday3" id="birthday3" class="profile-birthday-text" value="<?=$birthday3?>" maxlength="2">
                    </td>
                </tr>
                <tr>
                    <td class="profile-label">
                        <label for="hp1">휴대전화</label>
                    </td>
                    <td class="profile-input">
                        <select name="hp1" id="hp1" class="profile-small-select">
                            <?php foreach($hp_list as $label){?>
                            <option value="<?=$label?>" <?php if($hp1 == $label){ ?>selected<?php } ?>><?=$label?></option>
                            <?php } ?>
                        </select>
                        <input type="text" name="hp2" id="hp2" class="profile-small-text" value="<?=$hp2?>">
                        <input type="text" name="hp3" id="hp3" class="profile-small-text" value="<?=$hp3?>">
                    </td>
                    <td class="profile-label">
                        <label for="tel1">유선전화</label>
                    </td>
                    <td class="profile-input">
                        <select name="tel1" id="tel1" class="profile-small-select">
                            <option value="">::선택::</option>
                            <?php foreach($tel_list as $label){?>
                                <option value="<?=$label?>" <?php if($tel1 == $label){ ?>selected<?php } ?>><?=$label?></option>
                            <?php } ?>
                        </select>
                        <input type="text" name="tel2" id="tel2" class="profile-small-text" value="<?=$tel2?>">
                        <input type="text" name="tel3" id="tel3" class="profile-small-text" value="<?=$tel3?>">
                    </td>
                </tr>
                <tr>
                    <td class="profile-label">
                        <label for="zipcode">주소</label>
                    </td>
                    <td class="profile-input" colspan="3">
                        <input type="text" name="zipcode" id="zipcode" class="profile-zipcode" value="<?=$zipcode?>">
                        <button type="button" class="profile-zipcode-btn" onclick="openDaumPostCode()">우편번호 검색</button><br/>
                        <input type="text" name="address1" id="address1" class="profile-addr" value="<?=$address1?>"><br/>
                        <input type="text" name="address2" id="address2" class="profile-addr" value="<?=$address2?>">
                    </td>
                </tr>
                <tr>
                    <td class="profile-label">
                        <label for="email1">이메일</label>
                    </td>
                    <td class="profile-input" colspan="3">
                        <input type="text" name="email1" id="email1" class="profile-email-text" value="<?=$email1?>">
                        <span class="profile-email-tlt">@</span>
                        <input type="text" name="email2" id="email2" class="profile-email-text" value="<?=$email2?>">
                        <select name="email-sel" id="email-sel" class="profile-email-select" onchange="selEmailAddr()">
                            <option value="">::선택::</option>
                            <?php foreach($email_list as $label){?>
                                <option value="<?=$label?>"><?=$label?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="profile-label">
                        <label>전문분야</label>
                    </td>
                    <td class="profile-input" colspan="4">
                        <?php foreach($expert_list as $key => $label){?>
                            <input type="checkbox" name="expertise[]" id="expertise_<?=$key?>" class="profile-checkbox" value="<?=$key?>" <?php if($expertise_val && in_array($key, $expertise_val)){?>checked<?php }?>>
                            <label for="expertise_<?=$key?>" class="profile-checkbox-label"><?=$label?></label>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td class="profile-label" rowspan="2">
                        <label>학력</label>
                    </td>
                    <td class="profile-label">
                        <label for="university">대학교</label>
                    </td>
                    <td class="profile-input">
                        <input type="text" name="university" id="university" class="profile-text" value="<?=$university?>">
                    </td>
                    <td class="profile-label">
                        <label for="university_degree">학위취득명</label>
                    </td>
                    <td class="profile-input">
                        <input type="text" name="university_degree" id="university_degree" class="profile-text" value="<?=$university_degree?>">
                    </td>
                </tr>
                <tr>
                    <td class="profile-label">
                        <label for="graduate_school">대학원</label>
                    </td>
                    <td class="profile-input">
                        <input type="text" name="graduate_school" id="graduate_school" class="profile-text" value="<?=$graduate_school?>">
                    </td>
                    <td class="profile-label">
                        <label for="graduate_school_degree">학위취득명</label>
                    </td>
                    <td class="profile-input">
                        <input type="text" name="graduate_school_degree" id="graduate_school_degree" class="profile-text" value="<?=$graduate_school_degree?>">
                    </td>
                </tr>
                <tr>
                    <td class="profile-label plus-label-wrapper">
                        <label>
                            경력
                            <button type="button" class="plus-btn" onclick="addWrapperHtml('career')">+</button>
                        </label>
                    </td>
                    <td colspan="4" class="career-wrapper plus-wrapper">
                        <?php if(!empty($career_list)){?>
                            <?php foreach($career_list as $career){?>
                            <div class="career-input plus-input">
                                <div class="plus-div">
                                    <button type="button" class="minus-btn" onclick="delWrapperHtml(this)">x</button>
                                    <input type="hidden" name="lpc_no[]" value="<?=$career['lpc_no']?>">
                                    <label class="profile-label">직장명</label>
                                    <input type="text" name="lpc_name[]" class="profile-text" value="<?=$career['lpc_name']?>">
                                    <label class="profile-label">재직기간</label>
                                    <input type="text" name="lpc_s_period[]" class="profile-lpc-text month-picker" value="<?=$career['lpc_s_period']?>">
                                    <span class="profile-lpc-tlt"> ~ </span>
                                    <input type="text" name="lpc_e_period[]" class="profile-lpc-text month-picker" value="<?=$career['lpc_e_period']?>">
                                </div>
                                <div class="plus-div">
                                    <label class="profile-label">근무부서</label>
                                    <input type="text" name="lpc_department[]" class="profile-text" value="<?=$career['lpc_department']?>">
                                    <label class="profile-label">직위</label>
                                    <input type="text" name="lpc_position[]" class="profile-text" value="<?=$career['lpc_position']?>">
                                </div>
                                <div class="plus-div">
                                    <label class="profile-label">담당업무</label>
                                    <input type="text" name="lpc_work[]" class="profile-full-text" value="<?=$career['lpc_work']?>">
                                </div>
                            </div>
                            <?php } ?>
                       <?php }else{ ?>
                            <div class="career-input plus-input">
                                <div class="plus-div">
                                    <button type="button" class="minus-btn" onclick="delWrapperHtml(this)">x</button>
                                    <input type="hidden" name="lpc_no[]" value="">
                                    <label class="profile-label">직장명</label>
                                    <input type="text" name="lpc_name[]" class="profile-text" value="">
                                    <label class="profile-label">재직기간</label>
                                    <input type="text" name="lpc_s_period[]" class="profile-lpc-text month-picker" value="">
                                    <span class="profile-lpc-tlt"> ~ </span>
                                    <input type="text" name="lpc_e_period[]" class="profile-lpc-text month-picker" value="">
                                </div>
                                <div class="plus-div">
                                    <label class="profile-label">근무부서</label>
                                    <input type="text" name="lpc_department[]" class="profile-text" value="">
                                    <label class="profile-label">직위</label>
                                    <input type="text" name="lpc_position[]" class="profile-text" value="">
                                </div>
                                <div class="plus-div">
                                    <label class="profile-label">담당업무</label>
                                    <input type="text" name="lpc_work[]" class="profile-full-text" value="">
                                </div>
                            </div>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td class="profile-label plus-label-wrapper">
                        <label>
                            취득자격
                            <button type="button" class="plus-btn" onclick="addWrapperHtml('license')">+</button>
                        </label>
                    </td>
                    <td colspan="4" class="license-wrapper plus-wrapper">
                        <?php if(!empty($license_list)){?>
                            <?php foreach($license_list as $license){?>
                                <div class="license-input plus-input">
                                    <div class="plus-div">
                                        <button type="button" class="minus-btn" onclick="delWrapperHtml(this)">x</button>
                                        <input type="hidden" name="lpl_no[]" value="<?=$license['lpl_no']?>">
                                        <label class="profile-label">자격명</label>
                                        <input type="text" name="lpl_name[]" class="profile-text" value="<?=$license['lpl_name']?>">
                                        <label class="profile-label">발행기관</label>
                                        <input type="text" name="lpl_entity[]" class="profile-text" value="<?=$license['lpl_entity']?>">
                                    </div>
                                </div>
                            <?php } ?>
                        <?php }else{ ?>
                            <div class="license-input plus-input">
                                <div class="plus-div">
                                    <button type="button" class="minus-btn" onclick="delWrapperHtml(this)">x</button>
                                    <input type="hidden" name="lpl_no[]" value="">
                                    <label class="profile-label">자격명</label>
                                    <input type="text" name="lpl_name[]" class="profile-text" value="">
                                    <label class="profile-label">발행기관</label>
                                    <input type="text" name="lpl_entity[]" class="profile-text" value="">
                                </div>
                            </div>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td class="profile-label plus-label-wrapper">
                        <label>
                            강의/멘토링
                            <button type="button" class="plus-btn" onclick="addWrapperHtml('mentor')">+</button>
                        </label>
                    </td>
                    <td colspan="4" class="mentor-wrapper plus-wrapper">
                        <?php if(!empty($mentor_list)){?>
                            <?php foreach($mentor_list as $mentor){?>
                                <div class="mentor-input plus-input">
                                    <div class="plus-div">
                                        <button type="button" class="minus-btn" onclick="delWrapperHtml(this)">x</button>
                                        <input type="hidden" name="mentor_lpe_no[]" value="<?=$mentor['lpe_no']?>">
                                        <input type="hidden" name="mentor_lpe_type[]" value="<?=$mentor['lpe_type']?>">
                                        <input type="text" name="mentor_lpe_data[]" class="profile-full-text2" value="<?=$mentor['lpe_data']?>">
                                    </div>
                                </div>
                            <?php } ?>
                        <?php }else{ ?>
                            <div class="mentor-input plus-input">
                                <div class="plus-div">
                                    <button type="button" class="minus-btn" onclick="delWrapperHtml(this)">x</button>
                                    <input type="hidden" name="mentor_lpe_no[]" value="">
                                    <input type="hidden" name="mentor_lpe_type[]" value="1">
                                    <input type="text" name="mentor_lpe_data[]" class="profile-full-text2" value="">
                                </div>
                            </div>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td class="profile-label plus-label-wrapper">
                        <label>
                            기타
                            <button type="button" class="plus-btn" onclick="addWrapperHtml('etc')">+</button>
                        </label>
                    </td>
                    <td colspan="4" class="etc-wrapper plus-wrapper">
                        <?php if(!empty($etc_list)){?>
                            <?php foreach($etc_list as $etc){?>
                            <div class="etc-input plus-input">
                                <div class="plus-div">
                                    <button type="button" class="minus-btn" onclick="delWrapperHtml(this)">x</button>
                                    <input type="hidden" name="etc_lpe_no[]" value="<?=$etc['lpe_no']?>">
                                    <input type="hidden" name="etc_lpe_type[]" value="<?=$etc['lpe_type']?>">
                                    <input type="text" name="etc_lpe_data[]" class="profile-full-text2" value="<?=$etc['lpe_data']?>">
                                </div>
                            </div>
                            <?php } ?>
                        <?php }else{ ?>
                            <div class="etc-input plus-input">
                                <div class="plus-div">
                                    <button type="button" class="minus-btn" onclick="delWrapperHtml(this)">x</button>
                                    <input type="hidden" name="etc_lpe_no[]" value="">
                                    <input type="hidden" name="etc_lpe_type[]" value="2">
                                    <input type="text" name="etc_lpe_data[]" class="profile-full-text2" value="">
                                </div>
                            </div>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <h3 class="file-title">● 통장사본</h3>
                        <div class="file-wrapper">
                            <label class="profile-label">파일첨부</label>
                            <div class="file-content">
                                <?php if(empty($bankbook_path)){?>
                                    <span class="file-upload-text" id="bankbook-upload-text" style="display: none;"></span>
                                    <label for="bankbook_file" class="file-upload-btn">파일 업로드</label>
                                    <spna class="file-max-label">(2MB 이내)</spna>
                                    <input type="file" name="bankbook_file" id="bankbook_file" class="file-upload" attr-type="bankbook"/>
                                <?php }else{ ?>
                                    <a type="application/octet-stream" href="/uploads/<?=$bankbook_path?>" download="<?=$bankbook_name?>"><?=$bankbook_name?></a>
                                    <button type="button" onclick="del_file('bankbook', '<?=$bankbook_path?>');" class="btn_small">삭제</button>
                                <?php } ?>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="border-top: 0;">
                        <div class="bank-wrapper">
                            <label class="profile-label profile-bank-main-label">입금계좌</label>
                            <div class="bank-content-wrapper">
                                <label class="profile-label profile-bank-label">은행명</label>
                                <select name="bk_title" class="profile-bank-select" >
                                    <option value="">::선택::</option>
                                    <?php foreach($bank_list as $label){?>
                                        <option value="<?=$label?>" <?php if($bk_title == $label){ ?>selected<?php } ?>><?=$label?></option>
                                    <?php } ?>
                                </select>
                                <label class="profile-label profile-bank-label">계좌번호</label>
                                <input type="text" name="bk_num" class="profile-bank-text" id="bk_num" value="<?=$bk_num?>">
                                <label class="profile-label profile-bank-label">예금주</label>
                                <input type="text" name="bk_name" class="profile-bank-text" value="<?=$bk_name?>">
                                <span class="bank-notice font-blue" style="color: blue;">※ 반드시 통장사본과 입금계좌 정보가 일치해야 해당 계좌로 입금이 진행됩니다.</span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="4" style="padding: 5px 6px;"><span class="bank-notice font-blue" style="color: blue;">※ 계좌번호를 기재하실 때는 '-'를 포함하여 기재해 주세요.</span></td>
                </tr>
                <tr>
                    <td colspan="5">
                        <h3 class="file-title">● <?php if ($session_c_type == '3'){?>신분증<?php }else{ ?>사업자등록증<?php }?> 사본</h3>
                        <div class="file-wrapper">
                            <label class="profile-label">파일첨부</label>
                            <div class="file-content">
                                <?php if(empty($license_path)){?>
                                    <span class="file-upload-text" id="license-upload-text" style="display: none;"></span>
                                    <label for="license_file" class="file-upload-btn">파일 업로드</label>
                                    <spna class="file-max-label">(2MB 이내)</spna>
                                    <input type="file" name="license_file" id="license_file" class="file-upload" attr-type="license"/>
                                <?php }else{ ?>
                                    <a type="application/octet-stream" href="/uploads/<?=$license_path?>" download="<?=$license_name?>"><?=$license_name?></a>
                                    <button type="button" onclick="del_file('license', '<?=$license_path?>');" class="btn_small">삭제</button>
                                <?php } ?>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="4" style="padding: 5px 6px;"><span class="bank-notice" style="color: blue;">※ 원천세 신고를 위한 것으로, 블러처리 되지 않은 원본으로 제출하여 주세요.</span></td>
                </tr>
                <tr>
                    <td colspan="5">
                        <div class="profile-notice">
                            ※ 상기 내용은 사실과 다름이 없으며, 다름으로 인한 책임은 본인에게 있습니다.
                        </div>
                        <button type="submit" class="profile-submit-btn">저장하기</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    <form name="profile_file_frm" id="profile_file_frm" method="post" action="/profile/delFile">
        <input type="hidden" name="lp_no" value="<?=$lp_no?>">
        <input type="hidden" name="process" value="del_file">
        <input type="hidden" name="file_type" value="">
        <input type="hidden" name="file_path" value="">
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var active_li = $("#main-menu").find("li");
        $(active_li[2]).find("a").addClass('active');

        $("#profile-img").change(function(){
            var fileSize = (($(this)[0].files[0].size / 1024) / 1024);
            var fileName	= "";
            var fileExt		= "";

            jQuery.browser = {};
            if($.browser.msie){
                var objFSO 	 = new ActiveXObject("Scripting.FileSystemObject");
                var sPath 	 = $(this)[0].value;
                var objFile  = objFSO.getFile(sPath);
                fileName	 = $(this)[0].text;
                fileExt  	 = fileName.slice(fileName.indexOf('.') + 1).toLowerCase();
            }else {
                fileName 	 = $(this)[0].files[0].name;
                fileExt  	 = fileName.slice(fileName.indexOf('.') + 1).toLowerCase();
            }

            if(fileExt != 'jpg' && fileExt != 'jpeg'&& fileExt != 'png' && fileExt != 'gif'){
                $(this).val("");
                alert("JPG/JPEG , PNG, GIF 파일만 업로드 가능합니다.");
                return;
            }

            if(fileSize > 2) {
                $(this).val("");
                alert("2MB 이상 파일은 등록하실 수 없습니다.");
                return;
            }else{
                readURL(this);
            }
        });

        $(".file-upload").change(function(){
            var fileSize = (($(this)[0].files[0].size / 1024) / 1024);
            var fileName	= "";
            var fileExt		= "";

            jQuery.browser = {};
            if($.browser.msie){
                var objFSO 	 = new ActiveXObject("Scripting.FileSystemObject");
                var sPath 	 = $(this)[0].value;
                var objFile  = objFSO.getFile(sPath);
                fileName	 = $(this)[0].text;
                fileExt  	 = fileName.slice(fileName.indexOf('.') + 1).toLowerCase();
            }else {
                fileName 	 = $(this)[0].files[0].name;
                fileExt  	 = fileName.slice(fileName.indexOf('.') + 1).toLowerCase();
            }

            if(fileExt != 'jpg' && fileExt != 'jpeg'&& fileExt != 'png' && fileExt != 'gif'){
                $(this).val("");
                alert("JPG/JPEG , PNG, GIF 파일만 업로드 가능합니다.");
                return;
            }

            if(fileSize > 2) {
                $(this).val("");
                alert("2MB 이상 파일은 등록하실 수 없습니다.");
            }else{
                var attrType = $(this).attr('attr-type');
                var fileText = "#"+attrType+"-upload-text";

                $(fileText).text(fileName);
                $(fileText).show();
            }
        });
    });

    $(".month-picker").datepicker({
        format : "yyyy-mm",
        monthNames: ['1','2','3','4','5','6','7','8','9','10','11','12'],
        monthNamesShort:["1월","​2월","3월","4월","5월","6월","7월","8월","9월","10월","11월","12월"],
        startView: "months",
        minViewMode: "months",
        autoclose: true
    });

</script>
