<div class="mypage-wrapper">
    <h2>● My Page</h2>
    <form id="mypage_frm" name="mypage_frm" action="" method="post">
        <input type="hidden" name="c_no" value="<{$c_no}>">
        <input type="hidden" name="process" value="">
        <div class="form-group row"><!-- 투입가능여부(필수) -->
            <label class="col-form-label font-weight-bold" >ID</label>
            <div class="col-form-content">
                <span><?=$id?></span>
            </div>
        </div>
        <div class="form-group row"><!-- 비밀번호 -->
            <label class="col-form-label font-weight-bold pw-col-form-label" >비밀번호</label>
            <div class="col-form-content">
                <div>
                    <label for="partner_pw_new" class="form-input-label">변경 비밀번호</label>
                    <input type="password" name="partner_pw_new" id="partner_pw_new" class="form-control" ><br/>
                    <label for="partner_pw_confirm" class="form-input-label">변경 비밀번호 확인</label>
                    <input type="password" name="partner_pw_confirm" id="partner_pw_confirm" class="form-control" >
                    <span class="partner-pw-notice">※비밀번호 변경 시 입력 바랍니다.</span>
                </div>
                <button type="button" class="btn modify-pw-btn" onclick="changePassword()" >변경</button>
            </div>
        </div>
        <div class="form-group row"><!-- 강의/멘토링 참여여부 -->
            <label class="col-form-label font-weight-bold two-line" >강의/멘토링<br/>참여 여부</label>
            <div class="col-form-content">
                <div class="form-check form-check-inline">
                    <?php foreach($participation_list as $key => $label){ ?>
                    <input type="radio" name="participation" value="<?=$key?>" id="participation_<?=$key?>" class="form-check-input" onclick="save_el('modify_participation', this)" <?php if ((!$participation && $key == 1) || $participation == $key){?>checked<?php }?>>
                    <label for="participation_<?=$key?>" class="form-check-label" style="margin-right: 10px; cursor: pointer;"><?=$label?></label>
                    <?php }?>
                </div>
                <div class="participation-notice">※ 강사 프로필의 연락처를 통해 강의/멘토링 참여 요청 연락이 진행됩니다. 거부는 잠시 참여가 어려우실 경우 설정하시면 됩니다.</div>
            </div>
        </div>
        <div class="form-group row last"><!-- 회원탈퇴 -->
            <label class="col-form-label font-weight-bold" >회원탈퇴</label>
            <div class="col-form-content">
                <?php if($partner_state == '2'){?>
                <button type="button" class="btn leave-btn" onclick="moveLeavePartner()">회원탈퇴 신청</button>
                <?php }elseif($partner_state == '3'){?>
                <span>탈퇴대기 중입니다.</span>
                <?php }?>
            </div>
        </div>
    </form>
    <div class="btn-wrapper">
        <button type="button" class="logout-btn" onclick="logout()">로그아웃</button>
    </div>
</div>
