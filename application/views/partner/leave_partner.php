<div class="leave-wrapper">
    <h2>회원탈퇴 신청</h2>
    <form id="leave_frm" name="leave_frm" action="" method="post">
        <p>
            회원탈퇴 신청을 계속 진행하실 경우. 운영자가 최종 탈퇴처리를 진행하면서 등록한 정보가 모두 삭제 됩니다.<br/>
            이후 서비스를 이용하시려면, 기존 내역은 모두 사라지므로 새로 회원가입부터 시작해야 합니다.<br/><br/>
            그래도 탈퇴 하시겠습니까?
        </p>
        <textarea name="leave_reason" id="leave_reason" placeholder="회원탈퇴 사유를 적어주세요."></textarea>
        <div>
            <button type="button" id="cancel-leave-btn" onclick="cancelLeavePartner()">취소</button>
            <button type="button" id="approve-leave-btn" onclick="approveLeavePartner()">탈퇴 신청</button>
        </div>
    </form>
</div>