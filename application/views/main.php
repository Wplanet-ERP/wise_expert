<div class="main-wrapper">
    <?php if(!$is_partner_profile){ ?>
    <div class="profile-notice expert-warning">
        <img src="/images/tpl_bnr_01.png" usemap="#profile-map">
        <map name="profile-map" id="profile-map">
            <area shape="rect" coords="145,200,350,235" href="profile" target="_self">
        </map>
    </div>
    <?php } ?>
    <?php if(!$is_participation){ ?>
    <div class="participation-notice expert-warning">
        <img src="/images/tpl_bnr_02.png" usemap="#participation-map">
        <map name="participation-map" id="participation-map">
            <area shape="rect" coords="135,180,375,235" href="mypage" target="_self">
        </map>
    </div>
    <?php } ?>
    <div class="image-banner-wrapper">
        <img class="profile-logo" src="/images/tpl_bnr_03.png" >
    </div>
    <div class="project-table-wrapper">
        <table cellpadding="0" cellspacing="0" class="project-table">
            <colgroup>
                <col width="90px">
                <col width="230px">
                <col width="80px">
                <col width="80px">
            </colgroup>
            <thead>
            <tr>
                <td colspan="4">
                    <h2>강의 참여 리스트</h2>
                </td>
            </tr>
            <tr>
                <th>참여상태</th>
                <th>사업명</th>
                <th>참여 분야</th>
                <th>업체명<br/>(강사명)</th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($partner_project_list)){ ?>
                <?php foreach($partner_project_list as $partner_project){ ?>
                    <tr>
                        <td <?php if($partner_project['pj_participation'] == '1'){ ?>style="color: red;"<?php } ?>>
                            <?=$pj_participation_list[$partner_project['pj_participation']]?>
                        </td>
                        <td class="text-left"><?=$partner_project['pj_name'] ?></td>
                        <td><?=$resource_type_list[$partner_project['pj_c_type']]?></td>
                        <td><?=$partner_project['pj_c_name'] ?></td>
                    </tr>
                <?php }?>
            <?php }else{ ?>
                <tr>
                    <td colspan="4" class="project-table-empty">참여 중인 프로젝트가 없습니다</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <a href="/project_list" class="project-list-btn">더보기</a>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var active_li = $("#main-menu").find("li");
        $(active_li[0]).find("a").addClass('active');
    });
</script>