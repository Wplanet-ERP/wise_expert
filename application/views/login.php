<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />

    <title>Login Wise Expert</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- 합쳐지고 최소화된 최신 자바스크립트 -->
    <script src="https://code.jquery.com/jquery-1.9.1.min.js" charset="utf-8"></script>
    <script	src="../js/jquery-ui.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link href="../css/expert/regist.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker3.min.css">
    <script	type="text/javascript" src="../js/bootstrap/bootstrap-datepicker.min.js"></script>
    <script	type="text/javascript" src="../js/bootstrap/bootstrap-datepicker.ko.js"></script>

    <script	src="../js/jquery.chained.min.js"></script>
    <script	src="../js/jquery.chained.remote.min.js"></script>
    <script	src="../js/expert/login.js"></script>
</head>

<body>
<div id="continer">
    <form name="loginfrm" class="loginfrm" method="post" action="" onSubmit="return login()">
        <table class="login-total-table" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <td class="login-header">
                    <img class="login-logo" src="/images/wise_expert_logo.png"/>
                </td>
            </tr>
            <tr>
                <td class="login-input-wrapper">
                    <table cellpadding="0" cellspacing="0" class="login-input-table">
                        <tr>
                            <td>
                                <div class="m-b-5"><label for="user_id" class="idpw">ID</label></div>
                                <input type="text" name="user_id" id="user_id" class="input_w_login w300px" >
                                <div class="m-b-5"><label class="idpw">Password</label></div>
                                <input type="password" name="pw" id="pw" class="input_w_login w300px"><br>
                                <div class="login-etc-wrapper">
                                    <div class="id-save-wrap">
                                        <input type="checkbox" id="id_save"><label for="id_save">ID 저장</label>
                                    </div>
                                    <button class="btn btn-green btn-cons" type="submit">Login</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
    <div class="login-find-wrapper">
        <button type="button" class="find-btn" onclick="findBtn('regist')">강사등록(회원가입)</button>
        <button type="button" class="find-btn" onclick="findBtn('id')">아이디 찾기</button>
        <button type="button" class="find-btn" onclick="findBtn('pw')">비밀번호 찾기</button>
    </div>
