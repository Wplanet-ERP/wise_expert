<div class="project-list-wrapper">
    <div class="project-table-wrapper">
        <h2>● 강의 참여 리스트</h2>
        <table cellpadding="0" cellspacing="0" class="project-table">
            <colgroup>
                <col width="120px">
                <col width="250px">
                <col width="150px">
                <col width="80px">
                <col width="80px">
            </colgroup>
            <thead>
            <tr>
                <th>참여상태</th>
                <th>사업명</th>
                <th>강의/멘토링 확인서<br/>회신일시</th>
                <th>참여분야</th>
                <th>업체명<br/>(강사명)</th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($partner_project_list)): ?>
                <?php foreach($partner_project_list as $partner_project){ ?>
                <tr <?php if($partner_project['pj_participation'] == '3'){ ?>class="reject-pj-participation"<?php } ?>>
                    <td class="first">
                        <select name="pj_participation" class="form-control pj_participation" <?php if($partner_project['pj_participation'] == '1'){ ?>readonly<? }else{?>>disabled<?php }?>>
                            <?php foreach($pj_participation_list as $key => $label){ ?>
                            <option value="<?=$key?>" <?php if($partner_project['pj_participation'] == $key){ ?>selected<?php } ?> disabled><?=$label?></option>
                            <?php } ?>
                        </select>
                        <?php if($partner_project['is_participation'] == '1' && $partner_project['pj_participation'] == '1'){ ?>
                        <div>
                            <button type="button" class="approve-participation-btn" onclick="openParticipation('<?=$partner_project['pj_er_no']?>')">수락</button>
                            <button type="button" class="cancel-participation-btn" onclick="cancelParticipation('<?=$partner_project['pj_er_no']?>')">거절</button>
                        </div>
                        <?php } ?>
                    </td>
                    <td class="text-left"><span class="pj_name_<?=$partner_project['pj_er_no']?>"><?=$partner_project['pj_name']?></span></td>
                    <td><?=$partner_project['pj_participation_date']?></td>
                    <td><?=$partner_project['pj_c_type_name']?></td>
                    <td><?=$partner_project['pj_c_name']?></td>
                    <input type="hidden" class="pj_confirm_content_<?=$partner_project['pj_er_no']?>" value="<?=$partner_project['confirm_content']?>">
                </tr>
                <?php } ?>
            <?php else: ?>
            <tr>
                <td colspan="5" class="project-table-empty">참여 중인 프로젝트가 없습니다</td>
            </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal" id="pj-participation-modal">
    <div class="modal-content">
        <form name="part_frm" method="post" action="">
            <input type="hidden" name="pj_er_no" value="">
            <input type="hidden" name="process" value="">
            <h3 class="modal-pj-name">“<span class="modal-pj-name-span">경희대 빌드업 모델 프로그램</span>”</h3>
            <h2 class="modal-pj-label">강의/멘토링 확인서</h2>
            <div class="modal-pj-content">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td class="confirm-label">
                                <label for="confirm-name">성명</label>
                            </td>
                            <td class="confirm-input" colspan="2">
                                <input type="text" name="confirm_name" id="confirm-name" class="confirm-text" value="<?=$name?>" required>
                            </td>
                        </tr>
                        <tr>
                            <td class="confirm-label">
                                <label for="confirm-department">소속</label>
                            </td>
                            <td class="confirm-input" colspan="2">
                                <input type="text" name="confirm_department" id="confirm-department" class="confirm-text" value="<?=$department?>" required>
                                <label for="confirm-position" class="confirm-label confirm-position-label">직위</label>
                                <input type="text" name="confirm_position" id="confirm-position" class="confirm-text" value="<?=$position?>" required>
                            </td>
                        </tr>
                        <tr>
                            <td class="confirm-label" rowspan="2">
                                <label>연락처</label>
                            </td>
                            <td class="confirm-label">
                                <label for="confirm-hp1">휴대전화</label>
                            </td>
                            <td class="confirm-input">
                                <select name="confirm_hp1" id="confirm-hp1" class="confirm-small-select">
                                    <?php foreach($hp_list as $label){?>
                                        <option value="<?=$label?>" <?php if($hp1 == $label){ ?>selected<?php } ?>><?=$label?></option>
                                    <?php } ?>
                                </select>
                                <input type="text" name="confirm_hp2" id="confirm-hp2" class="confirm-small-text" maxlength="4" value="<?=$hp2?>" required>
                                <input type="text" name="confirm_hp3" id="confirm-hp3" class="confirm-small-text" maxlength="4" value="<?=$hp3?>" required>
                            </td>
                        </tr>
                        <tr>
                            <td class="confirm-label">
                                <label for="confirm-tel1">유선전화</label>
                            </td>
                            <td class="confirm-input">
                                <select name="confirm_tel1" id="confirm-tel1" class="confirm-small-select">
                                    <option value="">::선택::</option>
                                    <?php foreach($tel_list as $label){?>
                                        <option value="<?=$label?>" <?php if($tel1 == $label){ ?>selected<?php } ?>><?=$label?></option>
                                    <?php } ?>
                                </select>
                                <input type="text" name="confirm_tel2" id="confirm-tel2" class="confirm-small-text" maxlength="4" value="<?=$tel2?>" required>
                                <input type="text" name="confirm_tel3" id="confirm-tel3" class="confirm-small-text" maxlength="4" value="<?=$tel3?>" required>
                                <label for="confirm-email1" class="confirm-label confirm-email-label">E-mail</label>
                                <input type="text" name="confirm_email1" id="confirm-email1" class="confirm-email-text" value="<?=$email1?>" required>
                                <span class="confirm-email-tlt">@</span>
                                <input type="text" name="confirm_email2" id="confirm-email2" class="confirm-email-text" value="<?=$email2?>" required>
                            </td>
                        </tr>
                        <tr>
                            <td class="confirm-label">
                                <label for="confirm-notice">비고</label>
                            </td>
                            <td class="confirm-input" colspan="2">

                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="all-checkbox-wrapper">
                    <input type="checkbox" id="all-checkbox" value="1"/>
                    <label for="all-checkbox" id="all-checkbox-label">모두 동의합니다.</label>
                </div>
                <table cellpadding="0" cellspacing="0" class="confirm-check-table">
                    <tbody>
                        <tr>
                            <td colspan="2" class="confirm-check-label">확인서 관련 개인정보 수집ㆍ활용 및 제공에 대한 동의</td>
                            <td class="confirm-check-input confirm-check-approve-label">동의여부</td>
                        </tr>
                        <tr>
                            <td class="confirm-check-label">개인정보 수집 및 이용 (제공)</td>
                            <td class="confirm-check-content">
                                수집항목: 소속, 직위, 주소, 계좌번호, 연락처, 이메일, 성명<br/>
                                목적 : 강의․회의<br/>
                                보유기간 : 업무 수행 후 10년이내(공공기관 감사 관련 보유)<br/>
                            </td>
                            <td class="confirm-check-input">
                                <input type="checkbox" name="confirm_checkbox" class="confirm-checkbox" value="1" required/>
                            </td>
                        </tr>
                        <tr>
                            <td class="confirm-check-label">고유식별정보항목</td>
                            <td class="confirm-check-content">
                                수집항목: 주민등록번호<br/>
                                목적 : 세금신고 관련(소득세법 제145조·제164조)<br/>
                                보유기간 : 강의․회의 수행 후 10년이<br/>
                            </td>
                            <td class="confirm-check-input">
                                <input type="checkbox" name="confirm_checkbox" class="confirm-checkbox" value="1" required/>
                            </td>
                        </tr>
                        <tr>
                            <td class="confirm-check-label">개인정보 제3자 제공</td>
                            <td class="confirm-check-content">
                                강사비 지급 등을 위하여 개인정보를 3자에게 제공할 수 있음<br/>
                                수집항목　:소속, 직위, 주소, 계좌번호, 연락처, 이메일, 성명, 주민등록번호, 이력서<br/>
                                목적 : 강의, 회의 강의료 등 기타 비용의 세금신고 관련(소득세법 제 145조, 제 146조) 및 강사비 산정관련(자사 강사비 지급규정, 제공처 강사비 지급규정)<br/>
                                보유기간 : 강의․회의 수행 후 10년이내<br/>
                                제공처 : <span class="modal-pj-content-txt"></span><br/>
                            </td>
                            <td class="confirm-check-input">
                                <input type="checkbox" name="confirm_checkbox" class="confirm-checkbox" value="1" required/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="confirm-check-label">
                                귀하에게는 동의를 거부할 권리가 있으며, 거부할 경우 관련 업무를 진행하실 수 없습니다.<br/>
                                「개인정보보호법」등 관련 법규에 의거하여 상기 본인은 위의 개인정보취급 방침에 동의함.
                            </td>
                            <td class="confirm-check-input">
                                <input type="checkbox" name="confirm_checkbox" class="confirm-checkbox" value="1" required/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-last-label">
                상기 본인은 와이즈 플래닛에서 운영한 <span class="modal-pj-name">“<span class="modal-pj-name-span">경희대 빌드업 모델 프로그램</span>”</span>에 참여하는 것을 확인하였습니다.<br/>
                <span class="modal-date"><?=$cur_date?></span><br/>
                <span class="modal-req-name">성명: <?=$ss_name?></span>
            </div>
            <div class="modal-btn-wrapper">
                <button type="submit" onclick="approveParticipation()" class="approve-participation-btn">수락</button>
                <button type="button" onclick="refuseParticipation()" class="cancel-participation-btn">거절</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var active_li = $("#main-menu").find("li");
        $(active_li[1]).find("a").addClass('active');

        $("#all-checkbox").change(function(){
           if($(this).is(":checked")){
               $.each($(".confirm-checkbox"), function(index, item){
                   if($(item).is(":checked") === false){
                       $(item).click();
                   }
               });
           }else{
               $.each($(".confirm-checkbox"), function(index, item){
                   if($(item).is(":checked") === true){
                       $(item).click();
                   }
               });
           }
        });
    });
</script>
