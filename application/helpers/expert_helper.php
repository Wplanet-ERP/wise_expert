<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function getEmailList()
{
    $email_list = array(
        "naver.com" 	=> "naver.com",
        "gmail.com" 	=> "gmail.com",
        "hanmail.net" 	=> "hanmail.net",
        "hotmail.com" 	=> "hotmail.com",
        "korea.com" 	=> "korea.com",
        "nate.com" 		=> "nate.com",
        "yahoo.com" 	=> "yahoo.com",
        "empal.com" 	=> "empal.com",
        "paran.com" 	=> "paran.com",
        "yahoo.co.kr" 	=> "yahoo.co.kr",
    );

    return $email_list;
}

function getLicenseType()
{
    $license_type_list = array(
        '1' => "법인회사",
        '2' => "개인회사",
        '3' => "개인"
    );

    return $license_type_list;
}

function getBirthdayList()
{
    $birthday_list = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");

    return $birthday_list;
}

function getHpList()
{
    $hp_list = array("010", "011", "016", "017", "018", "019");

    return $hp_list;
}

function getTelList()
{
    $tel_list = array("02","070","051","053","032","062","042","052","044","031","033","043","041","063","061","054","055","064");

    return $tel_list;
}

function getExpertiseList()
{
    $expertise_list = array(
        "1" 	=> "선배창업가",
        "2" 	=> "경영 & 전략",
        "3" 	=> "마케팅 & 세일즈",
        "4" 	=> "기술개발",
        "5" 	=> "지식재산",
        "6" 	=> "세무/회계",
        "7" 	=> "투자",
        "8" 	=> "글로벌",
        "9" 	=> "진로 & 창업",
        "10" 	=> "기타",
        "11" 	=> "인사노무",
    );

    return $expertise_list;
}

function getParticipationList()
{
    $participation_list = array(
        "1" => "참여",
        "2" => "거부",
    );

    return $participation_list;
}

# 전문분야
function getResourceTypeList()
{
    $resource_type_list = array(
        'lector'    => '강사',
        'mentor'    => '멘토',
        'audit'     => '심사',
        'part_time' => '알바'
    );

    return $resource_type_list;
}

# 파트너 구분 배열
function getPjParticipation()
{
    $pj_participation_list = array(
        '1' => "참여 요청",
        '2' => "참여 수락",
        '3' => "참여 거절",
        '4' => "참여 취소"
    );

    return $pj_participation_list;
}

function folderCheck($folder)
{
    $rootImgFolderPath	 = $_SERVER['DOCUMENT_ROOT']."/uploads/".$folder;
    $folderNameY = date("Y"); //폴더 년도 생성
    $folderNameM = date("n"); //폴더 월 생성
    $folderName  = $folderNameY."/".$folderNameM;

    if(!is_dir($rootImgFolderPath."/".$folderNameY)){
        mkdir($rootImgFolderPath."/".$folderNameY, 0777);
    }

    if(!is_dir($rootImgFolderPath."/".$folderNameY."/".$folderNameM)){
        mkdir(($rootImgFolderPath."/".$folderNameY."/".$folderNameM), 0777);
    }

    return $folderName;
}

function store_image($file_type, $folder)
{
    $sub_folder_name = folderCheck($folder);

    $o_saveName = $_FILES[$file_type]["name"];
    $r_saveName = "";

    if($o_saveName){
        $tmp_filename = explode(".", $o_saveName);
        if ($tmp_filename[0] != '')
            $md5filename = md5(time().$tmp_filename[0]);
        else
            $md5filename = "";

        $ext = end($tmp_filename);
        $r_saveName = $folder. "/" . $sub_folder_name . "/" .$md5filename.".".$ext;

        move_uploaded_file($_FILES[$file_type]["tmp_name"], $_SERVER['DOCUMENT_ROOT']."/uploads/". $r_saveName );
    }

    return $r_saveName;
}

function del_image($file_path)
{
    $result = false;

    if($file_path)
    {
        $del_file    = $_SERVER['DOCUMENT_ROOT']."/uploads/{$file_path}";
        if(file_exists($del_file))
        {
            if(unlink($del_file)){
                $result = true;
            }
        }
    }

    return $result;
}

function GenerateString($length)
{
    $characters  = "0123456789";
    $characters .= "abcdefghijklmnopqrstuvwxyz";
    $characters .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $characters .= "_";

    $string_generated = "";

    $nmr_loops = $length;
    while ($nmr_loops--)
    {
        $string_generated .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string_generated;
}

function sendSms($sms_model, $hp, $name, $password)
{
    $sms_msg     = "비밀번호가 발급되었습니다.\r\n임시비밀번호 : {$password}";
    $sms_msg_len = mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
    $msg_type    = ($sms_msg_len > 90) ? "5" : "0";
    $send_result = false;

    $cmid       = date('YmdHis').sprintf("%04d","1");
    $cur_date   = date('Y-m-d H:i:s');
    $send_data  = array(
        'MSG_TYPE'      => $msg_type,
        'CMID'          => $cmid,
        'REQUEST_TIME'  => $cur_date,
        'SEND_TIME'     => $cur_date,
        'DEST_NAME'     => $name,
        'DEST_PHONE'    => $hp,
        'SEND_NAME'     => "와이즈 익스퍼트",
        'SEND_PHONE'    => "02-830-1912",
        'SUBJECT'       => "임시비밀번호 발급",
        'MSG_BODY'      => $sms_msg,
        'CINFO'         => "10",
    );

    if($sms_model->insert($send_data)){
        $send_result = true;
    }

    return $send_result;
}

function getCompanyInitData($postData)
{
    $birthday1      = trim($postData['birthday1']);
    $birthday2      = trim($postData['birthday2']);
    $birthday3      = trim($postData['birthday3']);
    $birthday       = $birthday1."-".$birthday2."-".$birthday3;
    $hp1            = trim($postData['hp1']);
    $hp2            = trim($postData['hp2']);
    $hp3            = trim($postData['hp3']);
    $hp             = $hp1."-".$hp2."-".$hp3;
    $email1         = trim(addslashes($postData['email1']));
    $email2         = trim(addslashes($postData['email2']));
    $email          = $email1."@".$email2;
    $bk_title       = trim(addslashes($postData['bk_title']));
    $bk_name        = trim(addslashes($postData['bk_name']));
    $bk_num         = trim(addslashes($postData['bk_num']));

    $company_upd_data = array(
        "birthday"  => ($birthday != "--") ? $birthday : "",
        "tx_email"  => $email,
        "tel"       => ($hp != "--") ? $hp : "",
        "bk_title"  => $bk_title,
        "bk_name"   => $bk_name,
        "bk_num"    => $bk_num
    );

    return $company_upd_data;
}

function getLectorInitData($postData)
{
    $department     = trim(addslashes($postData['department']));
    $position       = trim(addslashes($postData['position']));
    $name           = trim(addslashes($postData['name']));
    $birthday1      = trim($postData['birthday1']);
    $birthday2      = trim($postData['birthday2']);
    $birthday3      = trim($postData['birthday3']);
    $birthday       = $birthday1."-".$birthday2."-".$birthday3;
    $hp1            = trim($postData['hp1']);
    $hp2            = trim($postData['hp2']);
    $hp3            = trim($postData['hp3']);
    $hp             = $hp1."-".$hp2."-".$hp3;
    $tel1           = trim($postData['tel1']);
    $tel2           = trim($postData['tel2']);
    $tel3           = trim($postData['tel3']);
    $tel            = $tel1."-".$tel2."-".$tel3;
    $zipcode        = trim($postData['zipcode']);
    $address1       = trim(addslashes($postData['address1']));
    $address2       = trim(addslashes($postData['address2']));
    $email1         = trim(addslashes($postData['email1']));
    $email2         = trim(addslashes($postData['email2']));
    $email          = $email1."@".$email2;
    $expertise      = !empty($postData['expertise']) ? implode(",", $postData['expertise']) : "";
    $university             = trim(addslashes($postData['university']));
    $university_degree      = trim(addslashes($postData['university_degree']));
    $graduate_school        = trim(addslashes($postData['graduate_school']));
    $graduate_school_degree = trim(addslashes($postData['graduate_school_degree']));

    $lector_profile_data = array(
        "department" => $department,
        "position"   => $position,
        "name"       => $name,
        "birthday"   => $birthday,
        "hp"         => ($hp != '--') ? $hp : "",
        "tel"        => ($tel != '--') ? $tel : "",
        "zipcode"    => $zipcode,
        "address1"   => $address1,
        "address2"   => $address2,
        "expertise"  => $expertise,
        "email"      => ($email != '@') ? $email : "",
        "university"             => $university,
        "university_degree"      => $university_degree,
        "graduate_school"        => $graduate_school,
        "graduate_school_degree" => $graduate_school_degree,
    );

    return $lector_profile_data;
}


function profileInitData($profile_data)
{
    $profile_convert = array(
        "session_c_type"    => $profile_data['ss_c_type'],
        "lp_no"             => isset($profile_data['lp_no']) ? $profile_data['lp_no'] : "",
        "profile_img_path"  => isset($profile_data['profile_img_path']) ? $profile_data['profile_img_path'] : "",
        "department"        => isset($profile_data['department']) ? $profile_data['department'] : "",
        "position"          => isset($profile_data['position']) ? $profile_data['position'] : "",
        "name"              => isset($profile_data['name']) ? $profile_data['name'] : "",
        "birthday"          => isset($profile_data['birthday']) ? $profile_data['birthday'] : "",
        "hp"                => isset($profile_data['hp']) ? $profile_data['hp'] : "",
        "tel"               => isset($profile_data['tel']) ? $profile_data['tel'] : "",
        "zipcode"           => isset($profile_data['zipcode']) ? $profile_data['zipcode'] : "",
        "address1"          => isset($profile_data['address1']) ? $profile_data['address1'] : "",
        "address2"          => isset($profile_data['address2']) ? $profile_data['address2'] : "",
        "email"             => isset($profile_data['email']) ? $profile_data['email'] : "",
        "expertise"         => isset($profile_data['expertise']) ? $profile_data['expertise'] : "",
        "university"                => isset($profile_data['university']) ? $profile_data['university'] : "",
        "university_degree"         => isset($profile_data['university_degree']) ? $profile_data['university_degree'] : "",
        "graduate_school"           => isset($profile_data['graduate_school']) ? $profile_data['graduate_school'] : "",
        "graduate_school_degree"    => isset($profile_data['graduate_school_degree']) ? $profile_data['graduate_school_degree'] : "",
        "bankbook_path"     => isset($profile_data['bankbook_path']) ? $profile_data['bankbook_path'] : "",
        "bankbook_name"     => isset($profile_data['bankbook_name']) ? $profile_data['bankbook_name'] : "",
        "license_path"      => isset($profile_data['license_path']) ? $profile_data['license_path'] : "",
        "license_name"      => isset($profile_data['license_name']) ? $profile_data['license_name'] : "",
    );

    return $profile_convert;
}

function getBankList()
{
    $bank_list = array('경남','광주','국민','기업','농협','대구','새마을금고','수협','스탠다드차타드','시티', '신한', '신협', '씨티', '외환', '우리', '우체국', '전북', '제일', '카카오뱅크', '하나');
    return $bank_list;
}

?>
