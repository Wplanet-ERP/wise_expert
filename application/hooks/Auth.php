<?php
class Auth
{
    function checkPermission()
    {
        $CI =& get_instance();
        $CI->load->library('session');
        $CI->load->helper('url');

        if(isset($CI->allow) && (is_array($CI->allow) === false || in_array($CI->router->method, $CI->allow) === false))
        {
            if (!$CI->session->userdata('ss_c_no'))
            {
                redirect('/login');
            }
        }
    }
}
?>