<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 저장
 */
class Lector extends CI_Model
{
    protected $_main_table          = "lector_profile";
    protected $_sub_table_career    = "lector_profile_career";
    protected $_sub_table_etc       = "lector_profile_etc";
    protected $_sub_table_license   = "lector_profile_license";

    function __construct() {
        parent::__construct();
    }

    function profileCheck()
    {
        $profile_chk_sql    = "SELECT count(lp_no) as cnt FROM lector_profile WHERE c_no='{$this->session->userdata('ss_c_no')}'";
        $profile_chk_query  = $this->db->query($profile_chk_sql);
        $profile_chk_result = $profile_chk_query->row_array();

        $profile_count = isset($profile_chk_result['cnt']) ? $profile_chk_result['cnt'] : 0;

        return $profile_count;
    }

    function load($lp_no)
    {
        $lector_query = $this->db->get_where($this->_main_table, array('lp_no' => $lp_no));
        return $lector_query->row_array();
    }

    function loadByCno($c_no)
    {
        $lector_query = $this->db->limit(1)->get_where($this->_main_table, array('c_no' => $c_no));
        return $lector_query->row_array();
    }

    function loadCarrier($lp_no)
    {
        $carrier_query = $this->db->get_where($this->_sub_table_career, array('lp_no' => $lp_no, 'lpc_display' => '1'));
        return $carrier_query->result_array();
    }

    function loadLicense($lp_no)
    {
        $license_query = $this->db->get_where($this->_sub_table_license, array('lp_no' => $lp_no, 'lpl_display' => '1'));
        return $license_query->result_array();
    }

    function loadEtc($lp_no)
    {
        $etc_query = $this->db->get_where($this->_sub_table_etc, array('lp_no' => $lp_no, 'lpe_display' => '1'));
        return $etc_query->result_array();
    }

    function insertMainData($ins_data)
    {
        if($this->db->insert($this->_main_table, $ins_data)){
            return true;
        }else{
            return false;
        }
    }

    function getLastId(){
        return $this->db->insert_id();
    }

    function updateMainData($upd_data, $lp_no)
    {
        if($this->db->update($this->_main_table, $upd_data, array('lp_no' => $lp_no))){
            return true;
        }else{
            return false;
        }
    }

    function initSubTable($lp_no)
    {
        $this->db->update($this->_sub_table_career, array("lpc_display" => "2"), array('lp_no' => $lp_no));
        $this->db->update($this->_sub_table_license, array("lpl_display" => "2"), array('lp_no' => $lp_no));
        $this->db->update($this->_sub_table_etc, array("lpe_display" => "2"), array('lp_no' => $lp_no));
    }

    # 경력 등록
    function insertCarrierData($ins_data)
    {
        $this->db->insert($this->_sub_table_career, $ins_data);
    }

    # 경력 수정
    function updateCarrierData($upd_data, $lpc_no)
    {
        $this->db->update($this->_sub_table_career, $upd_data, array('lpc_no' => $lpc_no));
    }

    # 자격증 등록
    function insertLicenseData($ins_data)
    {
        $this->db->insert($this->_sub_table_license, $ins_data);
    }

    # 자격증 수정
    function updateLicenseData($upd_data, $lpl_no)
    {
        $this->db->update($this->_sub_table_license, $upd_data, array('lpl_no' => $lpl_no));
    }

    # 기타 등록
    function insertEtcData($ins_data)
    {
        $this->db->insert($this->_sub_table_etc, $ins_data);
    }

    # 기타 수정
    function updateEtcData($upd_data, $lpe_no)
    {
        $this->db->update($this->_sub_table_etc, $upd_data, array('lpe_no' => $lpe_no));
    }

    # 파일 삭제
    function delFile($lp_no, $file_type)
    {
        $upd_data = array(
            "{$file_type}_name" => "",
            "{$file_type}_path" => "",
        );

        if($this->db->update($this->_main_table, $upd_data, array('lp_no' => $lp_no))){
            return true;
        }else{
            return false;
        }
    }
}