<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Project extends CI_Model
{
    protected $_main_table = "project_external_report";
    protected $_sub_table  = "project_external_report_confirm";

    function __construct() {
        parent::__construct();
    }

    function getMainList()
    {
        $cur_date = date('Y-m-d');
        $partner_projcet_sql = "
            SELECT 
              pj.pj_name,
              per.pj_c_name,
              per.pj_c_type,
              per.pj_participation
            FROM project_external_report as per 
            LEFT JOIN project as pj ON pj.pj_no=per.pj_no 
            WHERE per.pj_c_no='{$this->session->userdata('ss_c_no')}' AND per.active='1' AND per.pj_participation IN('1','2') 
            AND (pj.pj_s_date <= '{$cur_date}' AND pj.pj_e_date >= '{$cur_date}') AND pj.display='1'
            ORDER BY pj.pj_no DESC
        ";

        $partner_project_query = $this->db->query($partner_projcet_sql);
        $partner_project_list  = [];
        foreach($partner_project_query->result_array() as $partner_project){
            $partner_project_list[] = $partner_project;
        }
        return $partner_project_list;
    }

    function getProjectList($resource_type_list)
    {
        $partner_projcet_sql = "
            SELECT 
                pj.pj_name,
                pj.confirm_content,
                per.pj_er_no,
                per.pj_c_name,
                per.pj_c_type,
                per.pj_participation,
                per.pj_participation_date,
                (SELECT `c`.`participation` FROM company as `c` WHERE `c`.c_no=per.pj_c_no LIMIT 1) as is_participation
            FROM project_external_report as per 
            LEFT JOIN project as pj ON pj.pj_no=per.pj_no 
            WHERE per.pj_c_no='{$this->session->userdata('ss_c_no')}' AND per.active='1' AND pj.display='1' AND per.pj_participation > 0
            ORDER BY pj.pj_no DESC
        ";

        $partner_project_query = $this->db->query($partner_projcet_sql);
        $partner_project_list  = [];
        foreach($partner_project_query->result_array() as $partner_project){
            $partner_project['pj_c_type_name'] = isset($resource_type_list[$partner_project['pj_c_type']]) ? $resource_type_list[$partner_project['pj_c_type']] : "참여분야가 없습니다";
            $partner_project_list[] = $partner_project;
        }
        return $partner_project_list;
    }

    function updatePjParticipation($pj_er_no, $value)
    {
        $upd_data = array(
            "pj_participation" => $value,
            "pj_participation_date" => date('Y-m-d H:i:s')
        );
        if($this->db->update($this->_main_table, $upd_data, array('pj_er_no' => $pj_er_no))){
            return true;
        }else{
            return false;
        }
    }

    function insertPjParticipationConfirm($confirmData)
    {
        if($this->db->insert($this->_sub_table, $confirmData)){
            return true;
        }else{
            return false;
        }
    }
}