<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 저장
 */
class Partner extends CI_Model
{
    protected $_table_name = "company";

    function __construct() {
        parent::__construct();
    }

    function loadByIdPw($id, $pw)
    {
        $comp_login_sql    = "SELECT c_no, id, c_name, license_type, tx_email, tel  FROM company WHERE id = '{$id}' AND pass = '{$pw}' AND corp_kind='3' AND partner_state='2'";
        $comp_login_query  = $this->db->query($comp_login_sql);
        $comp_login_result = ($comp_login_query->num_rows() > 0) ? $comp_login_query->row_array() : [];

        return $comp_login_result;
    }

    function loadByCno($c_no)
    {
        $company_query = $this->db->limit(1)->get_where($this->_table_name, array('c_no' => $c_no));
        return $company_query->row_array();
    }

    function partnerCheck($id)
    {
        $partner_chk_sql   = "SELECT c_no FROM company WHERE id='{$id}' AND partner_state != '4'";
        $partner_chk_query = $this->db->query($partner_chk_sql);
        $partner_chk	   = $partner_chk_query->num_rows();

        return $partner_chk;
    }

   function load()
   {
       $partner_query = $this->db->get_where($this->_table_name, array('c_no' => $this->session->userdata('ss_c_no')));
       return $partner_query->row_array();
   }

    function insert($option)
    {
        $partner_data = array(
            'my_c_no'       => '1',
            'corp_kind'     => '3',
            's_no'          => '22',
            'id'            => $option['f_id'],
            'pass'          => $option['f_pass'],
            'c_name'        => $option['f_name'],
            'tel'           => $option['f_hp'],
            'license_type'  => $option['f_license_type'],
            'tx_email'      => $option['f_email'],
            'tx_company'        => $option['f_tx_company'],
            'tx_company_ceo'    => $option['f_tx_company_ceo'],
            'tx_company_number' => $option['f_tx_company_number'],
            'participation'     => $option['f_participation'],
            'ip'                => $option['f_participation'],
            'partner_state'     => '2',
            'regdate'           => date('Y-m-d H:i:s')
        );

        if($this->db->insert($this->_table_name, $partner_data)) {
            return true;
        }else{
            return false;
        }
    }

    function partUpdate($value)
    {
        $upd_data = array("participation" => $value);

        if($this->db->update($this->_table_name, $upd_data, array('c_no' => $this->session->userdata('ss_c_no')))){
            return true;
        }else{
            return false;
        }
    }

    function updatePartnerData($upd_data)
    {
        $this->db->update($this->_table_name, $upd_data, array('c_no' => $this->session->userdata('ss_c_no')));
    }

    function partnerChangePassword($c_no, $password)
    {
        $change_password = substr(md5($password),8,16);
        $upd_data = array("pass" => $change_password);

        if($this->db->update($this->_table_name, $upd_data, array('c_no' => $c_no))){
            return true;
        }else{
            return false;
        }
    }

    function leavePartner($value)
    {
        $leave_reason   = addslashes(trim($value));
        $leave_data     = array("partner_state" => "3", "leave_reason" => $leave_reason, "leave_date" => date('Y-m-d H:i:s'), "display" => "2");

        if($this->db->update($this->_table_name, $leave_data, array('c_no' => $this->session->userdata('ss_c_no')))){
            return true;
        }else{
            return false;
        }
    }

    function findId($email, $c_name)
    {
        $partner_query = $this->db->get_where($this->_table_name, array('tx_email' => $email, 'c_name' => $c_name, 'corp_kind' => '3'));
        return $partner_query->row_array();
    }

    function findPw($id, $c_name)
    {
        $partner_query = $this->db->get_where($this->_table_name, array('id' => $id, 'c_name' => $c_name, 'corp_kind' => '3'));
        return $partner_query->row_array();
    }
}
