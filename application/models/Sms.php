<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 저장
 */
class Sms extends CI_Model
{
    protected $_table_name = "z_uds_msg";

    function __construct() {
        parent::__construct();
    }

    function insert($sms_data)
    {
        if($this->db->insert($this->_table_name, $sms_data)) {
            return true;
        }else{
            return false;
        }
    }
}