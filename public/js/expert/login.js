$(document).ready(function(){
    var user_id = getCookie("ic_user_id");

    $("#user_id").val(user_id);

    if($("#user_id").val() != ""){
        $("#id_save").attr("checked", true);
    }

    $("#user_id").focus();
});

function login() {
    var frm = document.loginfrm;

    if (frm.user_id.value.length == 0) {
        alert("아이디를 입력해 주세요.");
        frm.user_id.select();
        frm.user_id.focus();
        return false;
    }

    if (frm.pw.value.replace(/ /g, '') == "") {
        alert("비밀번호를 입력해 주세요.");
        frm.pw.focus();
        return false;
    }

    var user_id = $("#user_id").val();

    if($("#id_save").is(":checked")){
        setCookie("ic_user_id", user_id, 7);
    }else {
        deleteCookie("ic_user_id");
    }

    frm.action = '/login/loginPost';
    return true;
}

function setCookie(cookieName, value, exdays){
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
    document.cookie = cookieName + "=" + cookieValue;
}

function deleteCookie(cookieName){
    var expireDate = new Date();
    expireDate.setDate(expireDate.getDate() - 1);
    document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString();
}

function getCookie(cookieName) {
    cookieName = cookieName + '=';
    var cookieData = document.cookie;
    var start = cookieData.indexOf(cookieName);
    var cookieValue = '';
    if(start != -1){
        start += cookieName.length;
        var end = cookieData.indexOf(';', start);
        if(end == -1)end = cookieData.length;
        cookieValue = cookieData.substring(start, end);
    }
    return unescape(cookieValue);
}

function findBtn(type)
{
    if(type == 'regist'){
        location.href="/regist";
    }else{
        location.href="/find/find_"+type;
    }
}