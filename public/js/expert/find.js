function findId()
{
    var han_validator = /^[가-힣]+$/;
    var f_name = $("#f_name").val();
    if(!han_validator.test(f_name)){
        alert("이름은 한글만 입력해주세요");
        return false;
    }

    document.findIdFrm.action ="/find/findPost";
}

function searchEmailHp()
{
    var name  = $("#f_name").val();
    var id    = $("#f_id").val();

    if(id == ""){
        alert("아이디를 입력해주세요");
        $("#f_id").focus();
        return;
    } else if(name == ""){
        alert("이름을 입력해주세요");
        $("#f_name").focus();
        return;
    }else{
        $.ajax({
            url: "/find/companyPwSearch",
            type: "POST",
            data: {"id": id, "name": name}
        }).done( function( data ) {
            console.log(data);
            var jsonData = JSON.parse(data);
            var result	 = jsonData.result;
            var email	 = jsonData.email;
            var sms	     = jsonData.sms;

            if(result == true){
                $("#sch_email").val(email);
                $("#sch_hp").val(sms);

                $("#pw-search-btn").attr('disabled', true);
                if(email && sms){
                    $(".pw-send-btn").removeAttr('disabled');
                }else if(email){
                    $("#pw-send-email-btn").removeAttr('disabled');
                }else if(sms){
                    $("#pw-send-sms-btn").removeAttr('disabled');
                }else{
                    $("#pw-search-btn").removeAttr('disabled');
                    $(".pw-send-btn").attr('disabled', true);
                    alert("등록된 정보가 없습니다");
                }
            }else{
                $("#pw-search-btn").removeAttr('disabled');
                $(".pw-send-btn").attr('disabled', true);
                alert("등록된 정보가 없습니다");
            }
        });
    }
}

function sendFindPw(type)
{
    var f = document.findPwFrm;

    f.action = "/find/findPost";
    f.send_type.value = type;
    f.submit();
}

function confirmMoveUrl(type)
{
    location.href =  "/"+type;
}