$(document).ready(function(){
    $("#step-iframe",parent.document).height(1030);

    $("#f_id").change(function(){
        $("#f_id_chk").val(0);
    });

    $("#select-email").change(function(){
        $("#f_email2").val($(this).val());
    });

    $(".f_tuition_method").change(function(){
        var tuition_method = $(this).val();
        $(".f_license_type").attr('disabled', true);
        $(".f_license_type_label").css({'color': '#DDD'});
        if(tuition_method == '1'){
            $("#f_license_type_1").attr('disabled', false);
            $("#f_license_type_2").attr('disabled', false);
            $("#f_license_type_label_1").css({'color': '#4F5155'});
            $("#f_license_type_label_2").css({'color': '#4F5155'});
            $("#f_license_type_1").prop('checked', true);
            $("#f_license_type_1").trigger('change');
        }else if(tuition_method == '2'){
            $("#f_license_type_3").attr('disabled', false);
            $("#f_license_type_label_3").css({'color': "#4F5155"});
            $("#f_license_type_3").prop('checked', true);
            $("#f_license_type_3").trigger('change');
        }
    });

    $(".f_license_type").change(function(){
        var license_type = $(this).val();
        if(license_type == '1' || license_type == '2'){
            $(".f_license_type_detail").show();
            $("#step-iframe",parent.document).height(1180);
        }else{
            $(".f_license_type_detail").hide();
            $("#step-iframe",parent.document).height(1030);
        }
    });
});

function checkId()
{
    var id = $("#f_id").val();

    if(id == ""){
        alert("아이디를 입력해주세요");
        $("#f_id").focus();
        $("#f_id_chk").val(0);
        return;
    }else{
        $.ajax({
            url: "/regist/companyIdChk",
            type: "POST",
            data: {"id": id}
        }).done( function( data ) {
            var jsonData = JSON.parse(data);
            var result	 = jsonData.result;

            if(result == true){
                $("#f_id_chk").val(1);
                alert("사용가능한 아이디입니다.");
            }else{
                $("#f_id_chk").val(0);
                $("#f_id").val('');
                alert("이미 사용중인 아이디입니다.");
            }
        });
    }
}

function checkForm()
{
    var license_type = $(".f_license_type:checked").val();

    if(license_type == '1' || license_type == '2'){
        if(cknull("f_tx_company_number") == "x"){
            alert("사업자번호를 입력해주세요");
            return false;
        }

        if(cknull("f_tx_company") == "x"){
            alert("회사명을 입력해주세요");
            return false;
        }

        if(cknull("f_tx_company_ceo") == "x"){
            alert("대표자명을 입력해주세요");
            return false;
        }
    }

    if ($("#f_id_chk").val() < 1) {
        alert("아이디 중복체크를 해주세요");
        return false;
    }

    var alpha_num_validator = /^[a-zA-Z0-9]+$/;

    var f_id = $("#f_id").val();
    if(!alpha_num_validator.test(f_id)){
        alert("아이디는 영문 숫자만 입력해주세요");
        return false;
    }

    var f_password          = $("#f_password").val();
    var f_password_confirm  = $("#f_password_confirm").val();
    var password_validator  = /^[a-zA-Z0-9~!@#$%^&*]+$/;
    if((f_password.length < 6 || f_password.length > 20) || !password_validator.test(f_password))
    {
        alert("6~20자의 영문,숫자,특수문자(~!@#$%^&*)만 입력해주세요");
        return false;
    }

    if(f_password != f_password_confirm){
        alert("비밀번호가 맞지 않습니다. 다시 입력해주세요");
        $("#f_password_confirm").val('');
        $("#f_password_confirm").focus();
        return false;
    }

    var han_validator = /^[가-힣]+$/;
    var f_name = $("#f_name").val();
    if(!han_validator.test(f_name)){
        alert("이름은 한글만 입력해주세요");
        return false;
    }
}

function cknull(vid)
{
    if ($("#"+vid).val()=="") {
        $("#"+vid).focus();
        return "x";
    } else {
        return "o";
    }
}