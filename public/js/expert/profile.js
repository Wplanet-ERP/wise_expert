function del_file(folder, file)
{
    var f = document.profile_file_frm;
    f.process.value = 'del_file';
    f.file_type.value = folder;
    f.file_path.value = file;

    if (confirm("정말 삭제 하시겠습니까??") == true){ // 확인
        f.submit();
    }else{ //취소
        return;
    }
}

$(".date-picker").datepicker({
    format : "yyyy-mm-dd",
    autoclose: true
});

function selEmailAddr()
{
    $("#email2").val($("#email-sel").val());
}

function addWrapperHtml(type)
{
    var html = "";
    if(type == 'career')
    {
        html += "<div class='career-input plus-input'>"
            + "<div class='plus-div'>"
            + "<button type='button' class='minus-btn' onclick='delWrapperHtml(this)'>x</button>"
            + "<input type='hidden' name='lpc_no[]' value=''>"
            + "<label class='profile-label'>직장명</label>"
            + "<input type='text' name='lpc_name[]' class='profile-text' value=''>"
            + "<label class='profile-label'>재직기간</label>"
            + "<input type='text' name='lpc_s_period[]' class='profile-lpc-text month-picker' value=''>"
            + "<span class='profile-lpc-tlt'> ~ </span>"
            + "<input type='text' name='lpc_e_period[]' class='profile-lpc-text month-picker' value=''>"
            + "</div>"
            + "<div class='plus-div'>"
            + "<label class='profile-label'>근무부서</label>"
            + "<input type='text' name='lpc_department[]' class='profile-text' value=''>"
            + "<label class='profile-label'>직위</label>"
            + "<input type='text' name='lpc_position[]' class='profile-text' value=''>"
            + "</div>"
            + "<div class='plus-div'>"
            + "<label class='profile-label'>담당업무</label>"
            + "<input type='text' name='lpc_work[]' class='profile-full-text' value=''>"
            + "</div>"
            + "</div>"
        ;
    }else if(type == 'license')
    {
        html += "<div class='license-input plus-input'>"
            + "<div class='plus-div'>"
            + "<button type='button' class='minus-btn' onclick='delWrapperHtml(this)'>x</button>"
            + "<input type='hidden' name='lpl_no[]' value=''>"
            + "<label class='profile-label'>자격명</label>"
            + "<input type='text' name='lpl_name[]' class='profile-text' value=''>"
            + "<label class='profile-label'>발행기관</label>"
            + "<input type='text' name='lpl_entity[]' class='profile-text' value=''>"
            + "</div>"
            + "</div>"
        ;
    }
    else if(type == 'mentor')
    {
        html += "<div class='mentor-input plus-input'>"
            + "<div class='plus-div'>"
            + "<button type='button' class='minus-btn' onclick='delWrapperHtml(this)'>x</button>"
            + "<input type='hidden' name='mentor_lpe_no[]' value=''>"
            + "<input type='hidden' name='mentor_lpe_type[]' value='1'>"
            + "<input type='text' name='mentor_lpe_data[]' class='profile-full-text2' value=''>"
            + "</div>"
            + "</div>"
        ;
    }
    else if(type == 'etc')
    {
        html += "<div class='etc-input plus-input'>"
            + "<div class='plus-div'>"
            + "<button type='button' class='minus-btn' onclick='delWrapperHtml(this)'>x</button>"
            + "<input type='hidden' name='etc_lpe_no[]' value=''>"
            + "<input type='hidden' name='etc_lpe_type[]' value='2'>"
            + "<input type='text' name='etc_lpe_data[]' class='profile-full-text2' value=''>"
            + "</div>"
            + "</div>"
        ;
    }

    $("."+type+"-wrapper").append(html);

    if(type == 'career')
    {
        $(".month-picker").datepicker({
            format : "yyyy-mm",
            autoclose: true
        });
    }
}

function delWrapperHtml(obj){
    $(obj).parent().parent().remove();
}

function openDaumPostCode(type){
    new daum.Postcode({
        oncomplete: function(data) {
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;
            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }


            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            document.getElementById('zipcode').value = data.zonecode; //5자리 새우편번호 사용
            document.getElementById('address1').value = fullAddr;
            document.getElementById('address2').focus();
        }
    }).open();
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.preview-img-span').hide();
            $('.preview-img').attr('src', e.target.result);
            $('.preview-img').show();
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function changePassword()
{
    var password_validator  = /^[a-zA-Z0-9~!@#$%^&*]+$/;
    var new_pass = $("#partner_pw_new").val();
    var new_pass_confirm = $("#partner_pw_confirm").val();

    if((new_pass.length < 6 || new_pass.length > 20) || !password_validator.test(new_pass))
    {
        alert("6~20자의 영문,숫자,특수문자(~!@#$%^&*)만 입력해주세요");
        return false;
    }

    if(new_pass != new_pass_confirm){
        alert("비밀번호가 맞지 않습니다. 다시 입력해주세요");
        $("#partner_pw_confirm").val('');
        $("#partner_pw_confirm").focus();
        return false;
    }

    document.mypage_frm.action='/mypage/changePassword';
    document.mypage_frm.submit();
}

function moveLeavePartner()
{
    document.mypage_frm.action='/mypage/leavePartner';
    document.mypage_frm.submit();
}

function save_el(proc, obj)
{
    var val = obj.value;

    var x = $(obj).position();
    var d_msg = $('<div>').css({position:'absolute', top: '10px', right: '70px', width:'150px', textAlign:'left', fontWeight:'normal', background:'#d2e9ff', color:'#000', padding:'5px'});

    if ($(obj).prop('disabled') == true || $(obj).prop('readonly') == true)
        return false;

    if (val != "")
    {
        $.post("/mypage/save_el", {process : proc, value : val}, function(result){
            d_msg.appendTo($(obj).closest('.col-form-content')).html(result).fadeOut(2000);
        });
    }
}

function cancelLeavePartner()
{
    history.back();
}

function approveLeavePartner()
{
    if($("#leave_reason").val() == ""){
        alert("탈퇴 사유를 적어주세요.");
        $("#leave_reason").focus();
        return false;
    }

    document.leave_frm.action='/mypage/leavePost';
    document.leave_frm.submit();
}

function cancelParticipation(pj_er_no)
{
    if(confirm("참여 요청을 거절 합니까?"))
    {
        document.part_frm.action ='project_list/cancelParticipation';
        document.part_frm.pj_er_no.value=pj_er_no;
        document.part_frm.submit();
    }
}

function approveParticipation()
{
    document.part_frm.action  = 'project_list/approveParticipation';
}

function refuseParticipation()
{
    if(confirm("참여 요청을 거절 합니까?"))
    {
        document.part_frm.action  = 'project_list/cancelParticipation';
        document.part_frm.submit();
    }
}

function openParticipation(pj_er_no)
{
    $(".modal-pj-name-span").text($(".pj_name_"+pj_er_no).text());
    $(".modal-pj-content-txt").text($(".pj_confirm_content_"+pj_er_no).val());
    document.part_frm.pj_er_no.value = pj_er_no;

    $("#pj-participation-modal").modal({
        fadeDuration: 250
    });
}

function closeParticipationModal()
{
    $.modal.close();
    $(".modal-pj-name-span").text("");
    $(".modal-pj-content-txt").val("");
    document.part_frm.pj_er_no.value = "";
}

function logout()
{
    location.href='/logout';
}
